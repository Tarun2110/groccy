//
//  NetworkHelper.swift
//  MDM
//
//  Created by Amrit Sidhu on 1/15/18.
//  Copyright © 2018 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//



import UIKit
import SVProgressHUD


class NetworkHelper: NSObject {
//http://192.168.43.176:3000
    
   // "http://18.218.188.114/api/"
    
    //http://192.168.1.2:3000/
    //http://192.168.1.22:3000
   // http://192.168.43.176:3000/
    
   //  192.168.1.8
    var serviceUrl = URL.init(string: "http://18.218.188.114/api/")
    
    static let sharedInstance = NetworkHelper()
    func sendAyncronousRequest(method : String, requestType: String, params: Any?, showHud : Bool, completion: @escaping (_ result: Any) -> Void, failure:@escaping (NSError) -> Void) {
      
        if ReachabilityManager.shared.isAvailableNetwork() == true
        {
            if showHud == true
            {
                SVProgressHUD.show()
            }
            var request = URLRequest(url: URL.init(string: (String(format: "%@%@",  serviceUrl! as CVarArg, method)).addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!)!)
            let accessToken =  UserDefaults.standard.string(forKey: "accessToken")
            request.httpMethod = requestType
            request.timeoutInterval = 30.0
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.setValue (accessToken, forHTTPHeaderField: "x-access-token")
            
            if method != "login" || method != "signup"{
           }
            
            if params != nil{
                let jsonData = try? JSONSerialization.data(withJSONObject: params ?? "0", options: []);
                request.httpBody = jsonData
            }
            
            let task = URLSession.shared.dataTask(with: request) { data, response, error in
                guard let data = data, error == nil else {                                                 // check for fundamental networking error
                    
                    SVProgressHUD.dismiss()
                    OperationQueue.main.addOperation()
                        {
                            failure(error! as NSError)
                    }
                    return
                }
                
                if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {           // check for http errors
                    
                    SVProgressHUD.dismiss()
                    
                    OperationQueue.main.addOperation()
                    {
                        let errorTemp = NSError(domain:"", code:httpStatus.statusCode, userInfo:nil)
                        
                        failure(errorTemp)
                    }
                }
                else
                {
                     SVProgressHUD.dismiss()
                }
                
                
                do {
                    //create json object from data
                    if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
                        
                        SVProgressHUD.dismiss()
                        
                        var jsonString : String?
                        
                        jsonString = String.init(data: data, encoding: String.Encoding.utf8)
                        
                        print(jsonString ?? "")
                        
                        OperationQueue.main.addOperation()
                            {
                                completion(json as NSDictionary)
                        }
                    }
                } catch let error {
                    OperationQueue.main.addOperation()
                        {
                            SVProgressHUD.show()
                            
                            failure(error as NSError)
                    }
                }
                
            }
            task.resume()
        }
        else
        {
            let alert = UIAlertController.init(title: "Internet failure", message: "You are not connected with internet", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction.init(title: "OK", style: .default, handler: nil))
            
            let topWindow: UIWindow = UIApplication.shared.windows.last!
        
            topWindow.rootViewController?.present(alert, animated: true, completion: nil)
        }
    }
    
    
    class func alert(title: String, message :String, controller : UIViewController)
    {
        let alert = UIAlertController.init(title: title, message: message, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction.init(title: "OK", style: .default, handler: nil))
        
        controller.present(alert, animated: true, completion: nil)
    }
}
