//
//  ReachabilityManager.swift
//  Silence
//
//  Created by Seraphic Infosolutions Pvt. Ltd. on 1/3/18.
//  Copyright © 2018 Seraphic Infosolutions Pvt. Ltd. All rights reserved.
//

import UIKit
import ReachabilitySwift


class ReachabilityManager: NSObject {
 
    static  let shared = ReachabilityManager()
    
    var reachabilityStatus: Reachability.NetworkStatus = .notReachable
    
    var isNetworkAvailable : Bool {
        return reachabilityStatus != .notReachable
    }
    let reachability = Reachability()!
    
    
    @objc func reachabilityChanged(notification: Notification) {
        let reachability = notification.object as! Reachability
        switch reachability.currentReachabilityStatus {
       
        case .notReachable:
            let alert = UIAlertController.init(title: "Internet failure", message: "You are not connected with internet", preferredStyle: .alert)
            alert.addAction(UIAlertAction.init(title: "OK", style: .default, handler: nil))
            let topWindow: UIWindow = UIApplication.shared.windows.last!
            topWindow.rootViewController?.present(alert, animated: true, completion: nil)
            
            debugPrint("Network became unreachable")
        case .reachableViaWiFi:
            debugPrint("Network reachable through WiFi")
        case .reachableViaWWAN:
            debugPrint("Network reachable through Cellular Data")
        }
    }
    
    
    func startMonitoring() {
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.reachabilityChanged),
                                               name: ReachabilityChangedNotification,
                                               object: reachability)
        
        do{
            try reachability.startNotifier()
        } catch {
            debugPrint("Could not start reachability notifier")
        }
    }
    
    
    func isAvailableNetwork() -> Bool
    {
        let networkStatus: String = reachability.currentReachabilityString
      
        if networkStatus == "Cellular"
        {
            return true
        }
        else if networkStatus == "WiFi"
        {
            return true
        }
        else
        {
            return false
        }
    }
    
    func stopMonitoring(){
        reachability.stopNotifier()
        NotificationCenter.default.removeObserver(self,
                                                  name: ReachabilityChangedNotification,
                                                  object: reachability)
    }
}


