//
//  SideMenuVC.swift
//  SideMenuSwiftDemo
//
//  Created by Kiran Patel on 1/2/16.
//  Copyright © 2016  SOTSYS175. All rights reserved.
//

import Foundation
import UIKit
import MessageUI
class SideMenuVC: UIViewController, UITableViewDelegate, UITableViewDataSource,MFMailComposeViewControllerDelegate
{
    @IBOutlet var tableView: UITableView!
    
    @IBOutlet weak var selectedAreaLabel: UILabel!
    
   // UserDefaults.standard.set(true, forKey: "windowShop")

    
    let aData : NSArray = ["HOME","SHOP BY CATEGORY","My GROCCY ACCOUNT","GROCCY EXCLUSIVE PACK","MY ORDERS","BUY LATER BAG","ABOUT US","LIST ON GROCCY","COMMUNICATE","LOGOUT"]
    let imageArray : NSArray = ["home","menu_shopbycategory","profile","exclusive","cart","bag","about","list","communicate","login"]

    var deviceToken = ""
    
    
    override func viewDidLoad(){
        
       
        super.viewDidLoad()
        
       
        
        tableView.delegate = self
        tableView.dataSource = self
        selectedAreaLabel.text = UserDefaults.standard.value(forKey: "selectedArea") as? String
        self.tableView.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: #selector(catchNotification), name: NSNotification.Name(rawValue: "notificationName"), object: nil)

        
    }
    
    @objc func catchNotification(notification:Notification) -> Void {
        print("Catch notification")
        
       
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return aData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let aCell = tableView.dequeueReusableCell(
            withIdentifier: "kCell", for: indexPath)
        let aLabel : UILabel = aCell.viewWithTag(10) as! UILabel
        aLabel.text = aData[indexPath.row] as? String
        
        let Imagelink : UIImageView = aCell.viewWithTag(100) as! UIImageView
        Imagelink.image = UIImage(named: imageArray[indexPath.row] as! String)
        
        return aCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
          _ =  kConstantObj.SetIntialMainViewController("homeController")
        }else if indexPath.row == 1 {
          _ = kConstantObj.SetIntialMainViewController("shopByCategoryController")
        }else if indexPath.row == 2 {
            if  (UserDefaults.standard.bool(forKey: "windowShop")){
                AFWrapperClass.alert(Constants.ApplicationName, message: "Please log in.", view: self)
            }
            else{
                 _ =   kConstantObj.SetIntialMainViewController("groccyAccountController")
            }
          
        }
        else if indexPath.row == 3 {
            UserDefaults.standard.set(false, forKey: "fromHome")
           _ =   kConstantObj.SetIntialMainViewController("packageController")
        }
        else if indexPath.row == 4 {
            _ =  kConstantObj.SetIntialMainViewController("myOrdersController")
        }
        else if indexPath.row == 5 {
           _ =   kConstantObj.SetIntialMainViewController("wishlistController")
        }
        else if indexPath.row == 6 {
            _ =  kConstantObj.SetIntialMainViewController("aboutController")
        }
        else if indexPath.row == 7 {
            
            if MFMailComposeViewController.canSendMail() {
                let mail = MFMailComposeViewController()
                mail.mailComposeDelegate = self
                mail.setToRecipients(["support@groccy.com"])
                mail.setSubject("I wish to list my product on Groccy.")
                mail.setMessageBody("If you wish to sell your product on Groccy, Kindly mail us the details with a few pictures of the product and the description. \n We will get back to you. \nRegards \nTeam Groccy", isHTML: false)
                present(mail, animated: true)
            } else {
            }
        }
        else if indexPath.row == 8 {
           _ =   kConstantObj.SetIntialMainViewController("communicationController")
        }
        else if indexPath.row == 9 {
            UserDefaults.standard.set(false, forKey: "LoggedIn")
            if(UserDefaults.standard.value(forKey:"deviceToken") == nil)
            {
                print("NULL")
                deviceToken = ""
            }else{
                deviceToken  = UserDefaults.standard.value(forKey: "deviceToken") as! String
            }
            self.apiOTPLogout()
        }
    }
    
    func apiOTPLogout() {
        
        let params = [
            
            "deviceToken" : deviceToken,
            "deviceType": "ios"
            ] as [String : Any]
   
        NetworkHelper.sharedInstance.sendAyncronousRequest(method: "auth/signout", requestType: "POST", params: params as NSDictionary, showHud: false, completion: { (result) in
            if let dataDict = result as? NSDictionary, let status = dataDict["status"] as? Int, status == 1{
                _ = kConstantObj.SetIntialMainViewController("landingController")

            }
            else{
                let dataDict = result as? NSDictionary
                let message = dataDict?.value(forKey: "message") as? String
                AFWrapperClass.alert(Constants.ApplicationName, message:message!, view: self)
            }
        }) { (error) in
            AFWrapperClass.alert(Constants.ApplicationName, message: error.localizedDescription, view: self)
        }
    }
    
    
    
    func mailComposeController(_ controller: MFMailComposeViewController,
                               didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }

}
