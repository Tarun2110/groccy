//  cartController.swift
//  SwiftDemo
//  Created by Rakesh Kumar on 20/02/18.
//  Copyright © 2018 Rakesh Kumar. All rights reserved.

import UIKit

class cartCell: UITableViewCell {
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var sizeLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var totalPrice: UILabel!
    @IBOutlet weak var minusButton: UIButton!
    @IBOutlet weak var plusButton: UIButton!
    @IBOutlet weak var removeButton: UIButton!
    @IBOutlet weak var buyLaterButton: UIButton!
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var quantityLabel: UILabel!
    //Cell2
    @IBOutlet weak var bagTotalLabel: UILabel!
    @IBOutlet weak var couponDiscountLabel: UILabel!
    @IBOutlet weak var deliveryChargesLabel: UILabel!
    @IBOutlet weak var totalPayableLabel: UILabel!
}



class cartController: UIViewController,UITableViewDataSource,UITableViewDelegate{
    var productsCart = [CartProductModel]()
    
    @IBOutlet weak var headerTotalLabel: UILabel!
    @IBOutlet weak var headerItemsLabel: UILabel!
    @IBOutlet weak var proceedButton: UIButton!
    var couponsdiscount = ""
    var totalAmount = ""
    var selectedProductId = ""
    
    let cellReuseIdentifier = "Cell"
    @IBOutlet weak var cartTableView: UITableView!
    override func viewDidLoad(){
        super.viewDidLoad()
        self.proceedButton.isHidden = true
        self.navigationController?.isNavigationBarHidden = true
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.apiGetWishList()
    }
    
    
    @IBAction func tappedAction_menubutton(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if productsCart.count > 0 {
            return 3
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return productsCart.count
        }
        else{
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell:cartCell = self.cartTableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! cartCell!
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            let productType = productsCart[indexPath.row].type
            let product = productsCart[indexPath.row]
            
            cell.minusButton.tag = indexPath.row
            cell.plusButton.tag = indexPath.row
            
            cell.removeButton.tag = indexPath.row
            cell.buyLaterButton.tag = indexPath.row
            
            cell.removeButton.addTarget(self, action: #selector(self.removeButtonClicked(_:)), for:.touchUpInside)
            cell.buyLaterButton.addTarget(self, action: #selector(self.buyLaterButtonClicked(_:)), for:.touchUpInside)
            
            if productType == "product"{
                let imageProduct = (product.product.product_images.count > 0) ? product.product.product_images[0].image : ""
                
                cell.productName?.text = productsCart[indexPath.row].product.name
                let a:Int? = Int(productsCart[indexPath.row].quantity)
                let b:Int? = Int(productsCart[indexPath.row].selectedVarient.selling_price)
                let c =  Float(a!)*Float(b!)
                
                cell.totalPrice?.text = String(format:"Total: %0.2f",c)
                cell.sizeLabel?.text = "Size: " + productsCart[indexPath.row].selectedVarient.quantity.description
                cell.priceLabel?.text = "Price: " + productsCart[indexPath.row].selectedVarient.selling_price
                cell.quantityLabel?.text = productsCart[indexPath.row].quantity.description
                cell.productImage.sd_setImage(with: URL(string: imageProduct), placeholderImage: UIImage(named: "placeholder.png"))
                
                cell.plusButton.addTarget(self, action: #selector(self.plusButtonClicked(_:)), for:.touchUpInside)
                cell.minusButton.addTarget(self, action: #selector(self.minusButtonClicked(_:)), for:.touchUpInside)
                
                cell.minusButton.isHidden = false
                cell.plusButton.isHidden = false
                cell.sizeLabel.isHidden = false
               
                cell.buyLaterButton.isHidden = false

            }
            else{
                
                let imageProduct = productsCart[indexPath.row].packages.icon_image
                cell.productImage.sd_setImage(with: URL(string: imageProduct), placeholderImage: UIImage(named: "placeholder.png"))
                
                cell.productName?.text = productsCart[indexPath.row].packages.packName
                cell.quantityLabel?.text = "Quantity: " + productsCart[indexPath.row].quantity.description
                
                cell.sizeLabel.text =  productsCart[indexPath.row].packages.short_description
                cell.priceLabel.text = "Price: " + productsCart[indexPath.row].packages.quantity.description
                cell.totalPrice.text =  productsCart[indexPath.row].packages.packheading
                
                cell.minusButton.isHidden = true
                cell.plusButton.isHidden = true
                cell.buyLaterButton.isHidden = true

            }
            return cell
        }
        else if indexPath.section == 1{
            let cell:cartCell = self.cartTableView.dequeueReusableCell(withIdentifier: "Cell2") as! cartCell!
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            return cell
        }
        else{
            let cell:cartCell = self.cartTableView.dequeueReusableCell(withIdentifier: "Cell3") as! cartCell!
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            cell.totalPayableLabel?.text = "Rs: " + totalAmount
            cell.couponDiscountLabel?.text = "Rs: " + couponsdiscount
            cell.bagTotalLabel?.text = self.headerTotalLabel.text!
            return cell
        }
    }
    
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let backview = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.size.width, height: 20))
        backview.backgroundColor = UIColor.clear
        let textLabel = UILabel.init(frame: CGRect.init(x: 10, y: 0, width: tableView.frame.size.width-20, height: 20))
        textLabel.font = textLabel.font.withSize(12)
        if section == 1 {
            textLabel.text = "Options"
        } else if section == 2 {
            textLabel.text = "Price Details"
        }
        backview.addSubview(textLabel)
        return backview
    }
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return 0
        }
        return 20
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 1{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "couponsController") as! couponsController
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        if  indexPath.section == 0 {
            return 160;
        }
        else if indexPath.section == 1{
            return 50;
        }
        else{
            return 140;
        }
    }
    
    @objc func plusButtonClicked(_ sender:UIButton){
        if productsCart[sender.tag].quantity < 5{
            productsCart[sender.tag].quantity += 1 ;
            self.apiUpdateCart()
        }
    }
    
    @objc func removeButtonClicked(_ sender:UIButton){
        selectedProductId = productsCart[sender.tag].productID
        self.apiRemoveProduct()
    }
    
    @objc func buyLaterButtonClicked(_ sender:UIButton){
        
        let productId = productsCart[sender.tag].product.productID
        let selectedVariantId = productsCart[sender.tag].selectedVarient._id
        let quantityToSave = productsCart[sender.tag].quantity
        
        print(productId)
        print(selectedVariantId)
        print(quantityToSave)

        self.apiAddToBuylater(productId: productId, variantId: selectedVariantId, quantity: quantityToSave)
        
    }
    
    @objc func minusButtonClicked(_ sender:UIButton){
        if productsCart[sender.tag].quantity > 1{
            productsCart[sender.tag].quantity -= 1 ;
            self.apiUpdateCart()
        }
    }
    
    
    
    @IBAction func TappedAction_proceedToCheckout(_ sender: Any) {
        UserDefaults.standard.set(headerTotalLabel.text, forKey: "BagTotal")
        UserDefaults.standard.set(couponsdiscount, forKey: "couponDiscount")
        UserDefaults.standard.set(totalAmount, forKey: "payableAmount")
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "checkoutController") as! checkoutController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @IBAction func tappedAction_changeAddress(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "addressesController") as! addressesController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func tappedAction_ChangeAddress(_ sender: Any) {
        
    }
    
    @IBAction func tappedAction_NewAddress(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "newAddressController") as! newAddressController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func apiGetWishList() {
        NetworkHelper.sharedInstance.sendAyncronousRequest(method: "user/cart", requestType: "GET", params: nil, showHud: true, completion: { (result) in
            if let dataDict = result as? NSDictionary, let status = dataDict["status"] as? Int, status == 1{
                if let cartArray = dataDict.value(forKey: "cart") as? NSArray {
                    let cartCount = cartArray.count
                    if cartCount > 0{
                        if let cartArray = dataDict.value(forKey: "cart") as? NSArray {
                            self.productsCart = CartProductModel().getPackageModel(arr: cartArray)
                            self.headerItemsLabel.text = "Items: " + (cartCount.description)
                            self.proceedButton.isHidden = false
                        }
                        if let amount = dataDict.value(forKey: "checkout_amount") as? Int{
                            self.totalAmount = amount.description
                        }
                        
                        if let coupon = dataDict.value(forKey: "coupons_discount") as? NSNumber{
                            self.couponsdiscount = coupon.description
                        }
                        
                        if let totalAmountToPay = dataDict.value(forKey: "amount") as? NSNumber{
                            self.headerTotalLabel.text =  "Rs: " + totalAmountToPay.description
                        }
                        
                        if let couponsArray = dataDict.value(forKey: "coupons") as? NSArray {
                              if couponsArray.count > 0{
                                print(couponsArray)
                            }
                            
                        
                        }
                        
                        if let addressesArray = dataDict.value(forKey: "addresses") as? NSArray {
                            if addressesArray.count > 0{
                                for data in addressesArray {
                                    if let dict = data as? NSDictionary {
                                        if let primary = dict["primary"] as? Int, primary == 1 {
                                            let lineOne = dict["line1"]
                                            let lineTwo = dict["line2"]
                                            UserDefaults.standard.set(lineOne, forKey: "lineOne")
                                            UserDefaults.standard.set(lineTwo, forKey: "lineTwo")
                                            UserDefaults.standard.set(true, forKey: "addressAvailable")
                                        }}
                                }}else{
                                UserDefaults.standard.set(false, forKey: "addressAvailable")
                            }
                        }
                        if let walletArray = dataDict.value(forKey: "wallet") as? NSDictionary {
                            let availableValue = walletArray.value(forKey: "available")
                            let totalvalue = walletArray["total"]
                            UserDefaults.standard.set(availableValue, forKey: "availableRedeemAmount")
                            UserDefaults.standard.set(totalvalue, forKey: "totalWalletAmount")
                        }
                        self.cartTableView.delegate = self
                        self.cartTableView.dataSource = self
                        self.cartTableView.reloadData()
                    }
                    else{
                        self.cartTableView.delegate = self
                        self.cartTableView.dataSource = self
                        self.cartTableView.reloadData()
                        self.headerTotalLabel.text = "Total Rs.: 0"
                        self.headerItemsLabel.text =  "Items: 0"
                        AFWrapperClass.alert(Constants.ApplicationName, message: "Cart Empty", view: self)
                        self.proceedButton.isHidden = true
                    }}
            }
            else{
            }
        }) { (error) in
            AFWrapperClass.alert(Constants.ApplicationName, message: error.localizedDescription, view: self)
        }
    }
    
    func apiRemoveProduct() {
        let StringMsg = "user/cart/" + selectedProductId
        NetworkHelper.sharedInstance.sendAyncronousRequest(method: StringMsg, requestType: "DELETE", params: nil, showHud: true, completion: { (result) in
            if let dataDict = result as? NSDictionary, let status = dataDict["status"] as? Int, status == 1{
                if let cartArray = dataDict.value(forKey: "cart") as? NSArray {
                    let cartCount = cartArray.count
                    if cartCount > 0{
                        if let cartArray = dataDict.value(forKey: "cart") as? NSArray {
                            self.productsCart = CartProductModel().getPackageModel(arr: cartArray)
                            self.headerItemsLabel.text = "Items: " + (cartCount.description)
                            self.proceedButton.isHidden = false
                            
                            self.cartTableView.delegate = self
                            self.cartTableView.dataSource = self
                            self.cartTableView.reloadData()
                        }
                        if let amount = dataDict.value(forKey: "checkout_amount") as? Int{
                            self.totalAmount =  amount.description
                        }
                        
                        if let coupon = dataDict.value(forKey: "coupons_discount") as? NSNumber{
                            self.couponsdiscount =  coupon.description
                        }
                        
                        if let totalAmountToPay = dataDict.value(forKey: "amount") as? NSNumber{
                            self.headerTotalLabel.text =  "Rs: " + totalAmountToPay.description
                        }
                    }
                    else{
                        self.productsCart = CartProductModel().getPackageModel(arr: cartArray)
                        self.cartTableView.delegate = self
                        self.cartTableView.dataSource = self
                        self.cartTableView.reloadData()
                        self.headerItemsLabel.text =  "Items: 0"
                       
                        AFWrapperClass.alert(Constants.ApplicationName, message: "Cart Empty", view: self)
                        self.proceedButton.isHidden = true
                       
                        UserDefaults.standard.set(cartCount.description, forKey: "cartCount")
                        if let amount = dataDict.value(forKey: "checkout_amount") as? Int{
                            self.headerTotalLabel.text =  "Rs: " + amount.description
                        }
                    }}
            }
            else{
            }
        }) { (error) in
            AFWrapperClass.alert(Constants.ApplicationName, message: error.localizedDescription, view: self)
        }
    }
    
    func apiUpdateCart() {
        let cartParams = NSMutableArray()
        for p in productsCart {
            let type = (p.type == "product") ? "product" : "package"
            if type == "product"{
                let params = ["product" : p.product.productID,
                              "type" : "product",
                              "variant" : p.selectedVarient._id,
                              "id" : p.productID,
                              "quantity" : p.quantity.description] as [String : Any]
                cartParams.add(params)
            }
            else{
                let params = ["package" : p.packages.packId,
                              "type" : "package",
                              "id" : p.productID,
                              "quantity" : p.quantity.description] as [String : Any]
                cartParams.add(params)
            }}
        
        NetworkHelper.sharedInstance.sendAyncronousRequest(method: "user/cart", requestType: "PUT", params: cartParams as Any, showHud: false, completion: { (result) in
            print(result)
            print("success")
            
            if let dataDict = result as? NSDictionary, let status = dataDict["status"] as? Int, status == 1{
                if let cartArray = dataDict.value(forKey: "cart") as? NSArray {
                    let cartCount = cartArray.count
                    if cartCount > 0{
                        if let cartArray = dataDict.value(forKey: "cart") as? NSArray {
                            self.productsCart = CartProductModel().getPackageModel(arr: cartArray)
                            self.headerItemsLabel.text = "Items: " + (cartCount.description)
                            self.proceedButton.isHidden = false
                        }
                        
                        if let amount = dataDict.value(forKey: "checkout_amount") as? Int{
                            self.totalAmount = amount.description
                        }
                        
                        if let coupon = dataDict.value(forKey: "coupons_discount") as? NSNumber{
                            self.couponsdiscount =  coupon.description
                        }
                        
                        if let totalAmountToPay = dataDict.value(forKey: "amount") as? NSNumber{
                            self.headerTotalLabel.text = "Rs: " + totalAmountToPay.description
                        }
                        self.cartTableView.delegate = self
                        self.cartTableView.dataSource = self
                        self.cartTableView.reloadData()
                    }
                    else{
                        self.productsCart = CartProductModel().getPackageModel(arr: cartArray)
                        self.cartTableView.delegate = self
                        self.cartTableView.dataSource = self
                        self.cartTableView.reloadData()
                        self.headerItemsLabel.text =  "Items: 0"
                        AFWrapperClass.alert(Constants.ApplicationName, message: "Cart Empty", view: self)
                        self.proceedButton.isHidden = true
                        UserDefaults.standard.set(cartCount.description, forKey: "cartCount")
                    }}
            }
            else{
            }
        }) { (error) in
            AFWrapperClass.alert(Constants.ApplicationName, message: error.localizedDescription, view: self)
        }
    }
    
    func apiAddToBuylater(productId:String, variantId:String, quantity: Int) {
        let parameters = [
            "product": productId,
            "variant": variantId ,
            "quantity": quantity,
            "remove_from_cart": 1] as [String : Any]
        print(parameters)
        NetworkHelper.sharedInstance.sendAyncronousRequest(method: "user/wishlist", requestType: "POST", params: parameters as Any, showHud: true, completion: { (result) in
            if let dataDict = result as? NSDictionary, let status = dataDict["status"] as? Int, status == 1{
                let message = dataDict.value(forKey: "message") as? String
                AFWrapperClass.alert(Constants.ApplicationName, message: message!, view: self)
            }
            else{
            }
        }) { (error) in
            AFWrapperClass.alert(Constants.ApplicationName, message: error.localizedDescription, view: self)
        }
    }
}



