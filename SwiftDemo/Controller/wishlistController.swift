//
//  wishlistController.swift
//  SwiftDemo
//
//  Created by Rakesh Kumar on 24/04/18.
//  Copyright © 2018 Rakesh Kumar. All rights reserved.
//

import UIKit


class wishlistCustomCell: UITableViewCell {
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var sizeLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var totalPrice: UILabel!
    @IBOutlet weak var minusButton: UIButton!
    @IBOutlet weak var plusButton: UIButton!
    @IBOutlet weak var removeButton: UIButton!
    @IBOutlet weak var addToCartButton: UIButton!
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var quantityLabel: UILabel!
}


class wishlistController: UIViewController,UITableViewDataSource,UITableViewDelegate{
    var wishListCart = [CartProductModel]()
    let cellReuseIdentifier = "Cell"

    @IBOutlet weak var wishlistTableView: UITableView!
    
    override func viewWillAppear(_ animated: Bool) {
        self.apiGetWishList()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return wishListCart.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
     
            let cell:wishlistCustomCell = self.wishlistTableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! wishlistCustomCell!
            cell.selectionStyle = UITableViewCellSelectionStyle.none

            let product = wishListCart[indexPath.row]
            
            cell.minusButton.tag = indexPath.row
            cell.plusButton.tag = indexPath.row
            
            cell.removeButton.tag = indexPath.row
            cell.addToCartButton.tag = indexPath.row
            
            cell.removeButton.addTarget(self, action: #selector(self.removeButtonClicked(_:)), for:.touchUpInside)
            cell.addToCartButton.addTarget(self, action: #selector(self.tappedAction_addToCart(_:)), for:.touchUpInside)
        
        
                let imageProduct = (product.product.product_images.count > 0) ? product.product.product_images[0].image : ""
                
                cell.productName?.text = wishListCart[indexPath.row].product.name
                let a:Int? = Int(wishListCart[indexPath.row].quantity)
                let b:Int? = Int(wishListCart[indexPath.row].selectedVarient.selling_price)
                let c =  Float(a!)*Float(b!)
                
                cell.totalPrice?.text = String(format:"Total: %0.2f",c)
                cell.sizeLabel?.text = "Size: " + wishListCart[indexPath.row].selectedVarient.quantity.description
                cell.priceLabel?.text = "Price: " + wishListCart[indexPath.row].selectedVarient.selling_price
                cell.quantityLabel?.text = "Quantity: " + wishListCart[indexPath.row].quantity.description
                cell.productImage.sd_setImage(with: URL(string: imageProduct), placeholderImage: UIImage(named: "placeholder.png"))
                
                cell.minusButton.isHidden = true
                cell.plusButton.isHidden = true
                cell.sizeLabel.isHidden = false
                cell.addToCartButton.isHidden = false
//            }
//            else{
//                let imageProduct = wishListCart[indexPath.row].packages.icon_image
//                cell.productImage.sd_setImage(with: URL(string: imageProduct), placeholderImage: UIImage(named: "placeholder.png"))
//
//                cell.productName?.text = wishListCart[indexPath.row].packages.packName
//                cell.quantityLabel?.text = "Quantity: " + wishListCart[indexPath.row].quantity.description
//
//                cell.sizeLabel.text =  wishListCart[indexPath.row].packages.short_description
//                cell.priceLabel.text = "Price: " + wishListCart[indexPath.row].packages.quantity.description
//                cell.totalPrice.text =  wishListCart[indexPath.row].packages.packheading
//
//                cell.minusButton.isHidden = true
//                cell.plusButton.isHidden = true
//                cell.addToCartButton.isHidden = true
//            }
            return cell
    }
    

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "couponsController") as! couponsController
            self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
            return 160;
    }

    
    func apiGetWishList() {
        NetworkHelper.sharedInstance.sendAyncronousRequest(method: "user/wishlist", requestType: "GET", params: nil, showHud: true, completion: { (result) in
            if let dataDict = result as? NSDictionary, let status = dataDict["status"] as? Int, status == 1{
                if let cartArray = dataDict.value(forKey: "wishlist") as? NSArray {
                    let cartCount = cartArray.count
                    if cartCount > 0{
                        if let cartArray = dataDict.value(forKey: "wishlist") as? NSArray {
                            self.wishListCart = CartProductModel().getPackageModel(arr: cartArray)
                        }
                        self.wishlistTableView.delegate = self
                        self.wishlistTableView.dataSource = self
                        self.wishlistTableView.reloadData()
                    }
                    else{
                        self.wishlistTableView.delegate = self
                        self.wishlistTableView.dataSource = self
                        self.wishlistTableView.reloadData()
                        AFWrapperClass.alert(Constants.ApplicationName, message: "wishlist Empty", view: self)
                    }}
            }
            else{
            }
        }) { (error) in
            AFWrapperClass.alert(Constants.ApplicationName, message: error.localizedDescription, view: self)
        }
    }
    
    
    func apiDeleteItemFromWishList(itemId : String) {
        let urllinked = "user/wishlist/" + itemId
        NetworkHelper.sharedInstance.sendAyncronousRequest(method: urllinked, requestType: "DELETE", params: nil, showHud: true, completion: { (result) in
            if let dataDict = result as? NSDictionary, let status = dataDict["status"] as? Int, status == 1{
                AFWrapperClass.alert(Constants.ApplicationName, message: dataDict["message"] as! String, view: self)
                self.apiGetWishList()
            }
            else{
            }
        }) { (error) in
            AFWrapperClass.alert(Constants.ApplicationName, message: error.localizedDescription, view: self)
        }
    }
    
    @objc func removeButtonClicked(_ sender: UIButton) {
        let product = wishListCart[sender.tag]
        let productId = product.productID
        self.apiDeleteItemFromWishList(itemId: productId)
    }
    
    
    @IBAction func tappedAction_backButton(_ sender: Any) {
        sideMenuVC.toggleMenu()
    }

    @objc func tappedAction_addToCart(_ sender: UIButton) {
        let product = wishListCart[sender.tag]
        let cartArr = NSMutableArray()
        let params = ["type" : "product",
                      "product" : product.product.productID,
                      "variant" : product.selectedVarient._id,
                      "quantity" : product.quantity] as [String : Any]
        cartArr.add(params)

        NetworkHelper.sharedInstance.sendAyncronousRequest(method: "user/cart", requestType: "POST", params: cartArr as Any, showHud: false, completion: { (result) in
            
        if let dataDict = result as? NSDictionary, let status = dataDict["status"] as? Int, status == 1{
            AFWrapperClass.alert(Constants.ApplicationName, message: "Item added to the cart", view: self)
            }
            else{
            }
        }) { (error) in
            AFWrapperClass.alert(Constants.ApplicationName, message: error.localizedDescription, view: self)
        }
    }
    
}
