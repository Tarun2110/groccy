//
//  buyItemsController.swift
//  SwiftDemo
//
//  Created by Rakesh Kumar on 20/02/18.
//  Copyright © 2018 Rakesh Kumar. All rights reserved.
//

import UIKit

var products = [ProductModel]()
var variants = [VariantsModel]()

protocol MyCollectionViewCellProtocol {
    func selectedVarieint(varient:VariantsModel, index:Int)
}


class MyCollectionViewCell: UICollectionViewCell {
    
    var delegate: MyCollectionViewCellProtocol?
    var varientPicker = UIPickerView()
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var productName: UILabel!
    
   
    @IBOutlet weak var productAddButton: UIButton!
    @IBOutlet weak var productBuyLaterButton: UIButton!
    @IBOutlet weak var ProductPrice: UILabel!
    @IBOutlet weak var varientTextfield: UITextField!
   
    @IBOutlet weak var totalAmount: UILabel!
    @IBOutlet weak var countStack: UIStackView!
    @IBOutlet weak var minusButton: UIButton!
    @IBOutlet weak var countLabel: UILabel!
    @IBOutlet weak var addbutton: UIButton!

    func setUpCell(index: Int) {
        varientPicker.delegate = self
        varientPicker.dataSource = self
        varientPicker.tag = index
        varientTextfield.delegate = self
        varientTextfield.inputView = varientPicker
        varientTextfield.tag = index
    }
}

extension MyCollectionViewCell: UITextFieldDelegate, UIPickerViewDelegate,UIPickerViewDataSource {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        variants = products[textField.tag].variants
        varientPicker.reloadAllComponents()
        return true
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return variants.count
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return variants[row].quantity
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        varientTextfield.text = variants[row].quantity
        ProductPrice.text = variants[row].selling_price
        if variants[row].selling_price != "" , let value = Double(variants[row].selling_price) {
            totalAmount.text =  "Rs. " + String(format:"%.f", value*Double(products[pickerView.tag].count))
        }
        delegate?.selectedVarieint(varient: variants[row], index: pickerView.tag)
    }
}


class buyItemsController: UIViewController,UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, MyCollectionViewCellProtocol {
    var CategoryID = ""
    var cartDictonary = [String: Any]()
    var CartArray = [Any]()
   
   // var animationCount = 0
  //  var animationArray = [Any]()

    
    @IBOutlet weak var buttonCart: UIButton!
    @IBOutlet weak var cartView: UIStackView!

    @IBOutlet weak var cartTotalLabel: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    private let cellReuseIdentifier = "collectionCell"
    @IBOutlet weak var addToCartButton: UIButton!
  
    @IBAction func tappedAction_menubutton(_ sender: Any) {
        let _ = self.navigationController?.popViewController(animated: true)
    }
    
    override func viewDidLoad(){
        super.viewDidLoad()

        let flowLayout = UICollectionViewFlowLayout()
        let collectionView = UICollectionView(frame: self.view.bounds, collectionViewLayout: flowLayout)
        self.addToCartButton.isHidden = true;
        collectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "collectionCell")
        collectionView.delegate = self
        collectionView.dataSource = self
        self.navigationController?.isNavigationBarHidden = true
        self.apiProductViaCategory()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.cartTotalLabel.text =  UserDefaults.standard.string(forKey: "cartCount")
     }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        return products.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellReuseIdentifier, for: indexPath) as! MyCollectionViewCell
        let product = products[indexPath.item]
        let imageProduct = (product.product_images.count > 0) ? product.product_images[0].image : ""
        
        if product.count == 0 {
            cell.countStack.isHidden = true
            cell.productAddButton.isHidden = false
            cell.productBuyLaterButton.isHidden = false
            cell.totalAmount.isHidden = true;
        } else {
            cell.productAddButton.isHidden = true
            cell.productBuyLaterButton.isHidden = true
            cell.countStack.isHidden = false
            cell.totalAmount.isHidden = false;
            cell.countLabel.text = product.count.description
            cell.addbutton.addTarget(self, action: #selector(self.addButtonClicked(_:)), for:.touchUpInside)
            cell.minusButton.addTarget(self, action: #selector(self.minusButtonClicked(_:)), for:.touchUpInside)
        }
        cell.productName.text = product.name
        cell.productImage.sd_setImage(with: URL(string: imageProduct), placeholderImage: UIImage(named: "placeholder.png"))
        cell.productAddButton.tag = indexPath.item
        cell.productBuyLaterButton.tag = indexPath.item
        cell.ProductPrice.text = "Rs. " + product.selectedVarient.selling_price
        
        if product.selectedVarient.selling_price != "" , let value = Double(product.selectedVarient.selling_price) {
            cell.totalAmount.text = "Rs. " + String(format:"%.f", value*Double(product.count))
        }
        
        if product.type == "" {
        }else{
        }
        
        cell.addbutton.tag = indexPath.item
        cell.minusButton.tag = indexPath.item
        cell.countLabel.tag =  indexPath.item
        cell.countStack.tag = indexPath.item
        cell.totalAmount.tag = indexPath.item
        
        let paddingView : UIView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 30))
        let paddingViewR : UIView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 30))
        
        let imageView = UIImageView(frame: CGRect(x: -10, y: 12.5, width: 10, height: 5))
        let image = UIImage(named: "arrow_down")
        imageView.image = image
        imageView.contentMode = .center
        
        paddingViewR.addSubview(imageView)
        cell.varientTextfield.rightView = paddingViewR
        cell.varientTextfield.leftView = paddingView
        cell.varientTextfield.leftViewMode = .always
        cell.varientTextfield.rightViewMode = .always
        
        cell.varientTextfield.text = product.selectedVarient.quantity
        cell.productAddButton.addTarget(self, action: #selector(self.buttonClicked(_:)), for:.touchUpInside)
        cell.productBuyLaterButton.addTarget(self, action: #selector(self.buylaterButtonClicked(_:)), for:.touchUpInside)

        cell.setUpCell(index: indexPath.item)
        cell.delegate = self
      
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "itemDetailController") as! itemDetailController
        vc.product = products[indexPath.item]
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width/2-5, height: 260)
    }
    
    
    
    @objc func buylaterButtonClicked(_ sender:UIButton){
        let p =  products[sender.tag]
        self.apiAddToBuylater(productId: p.productID, variantId: p.selectedVarient._id)
    }
    
    
    @objc func buttonClicked(_ sender:UIButton){
        products[sender.tag].count = 1 ;
        
     //   self.animationArray.append(sender)
        
        self.collectionView.reloadItems(at: [IndexPath.init(item: sender.tag, section: 0)])
        let ps = products.filter { (p) -> Bool in
            return p.count > 0
        }
        addToCartButton.isHidden = (ps.count > 0) ? false : true
    }
    

    @objc func addButtonClicked(_ sender:UIButton){
        if  products[sender.tag].count < 5 {
            products[sender.tag].count += 1 ;

            self.collectionView.reloadItems(at: [IndexPath.init(item: sender.tag, section: 0)])
            let ps = products.filter { (p) -> Bool in
                return p.count > 0
            }
            addToCartButton.isHidden = (ps.count > 0) ? false : true
        }
    }
    
    
    @objc func minusButtonClicked(_ sender:UIButton){
        products[sender.tag].count -= 1 ;
        self.collectionView.reloadItems(at: [IndexPath.init(item: sender.tag, section: 0)])
       
     //   self.animationArray.remove(at: sender.tag)
        let ps = products.filter { (p) -> Bool in
            return p.count > 0
        }
        addToCartButton.isHidden = (ps.count > 0) ? false : true
    }
    
    func selectedVarieint(varient: VariantsModel, index: Int) {
        products[index].selectedVarient = varient
    }

    func apiProductViaCategory(){
        let urlString = "category/" + self.CategoryID + "/products"
        NetworkHelper.sharedInstance.sendAyncronousRequest(method: urlString, requestType: "GET", params: nil, showHud: false, completion: { (result) in
      
            if let dataDict = result as? NSDictionary, let status = dataDict["status"] as? Int, status == 1{
                let productsDict = dataDict.value(forKey: "products") as? NSDictionary
                if let productArray = productsDict!["docs"] as? NSArray , productArray.count > 0 {
                    self.collectionView .isHidden = false;
                    products = ProductModel().getPackageModel(arr: productArray)
                    self.collectionView.reloadData()
                } else {
                    AFWrapperClass.alert(Constants.ApplicationName, message: "No Products", view: self)
                    self.collectionView .isHidden = true;
                }
            }
            else{
            }
        }) { (error) in
            AFWrapperClass.alert(Constants.ApplicationName, message: error.localizedDescription, view: self)
        }
    }
    
    @IBAction func tapped_addToCart(_ sender: Any) {
            //animationCount = 0
            animationFlyToCart()
            self.apiAddCart()
    }
    
    func apiAddToBuylater(productId:String, variantId:String) {
        let parameters = [
        "product": productId,
        "variant": variantId ,
        "quantity": 1,
        "remove_from_cart": 0] as [String : Any]
        print(parameters)
        NetworkHelper.sharedInstance.sendAyncronousRequest(method: "user/wishlist", requestType: "POST", params: parameters as Any, showHud: true, completion: { (result) in
            if let dataDict = result as? NSDictionary, let status = dataDict["status"] as? Int, status == 1{
                let message = dataDict.value(forKey: "message") as? String
                AFWrapperClass.alert(Constants.ApplicationName, message: message!, view: self)
            }
            else{
            }
        }) { (error) in
            AFWrapperClass.alert(Constants.ApplicationName, message: error.localizedDescription, view: self)
        }
    }
    
   func apiAddCart() {
        let cartArr = NSMutableArray()
        let prdts = products.filter { (p) -> Bool in
            return p.count > 0
        }
        for p in prdts {
            let params = ["type" : "product",
                          "product" : p.productID,
                          "variant" : p.selectedVarient._id,
                          "quantity" : p.count.description] as [String : Any]
            cartArr.add(params)
        }
        NetworkHelper.sharedInstance.sendAyncronousRequest(method: "user/cart", requestType: "POST", params: cartArr as Any, showHud: false, completion: { (result) in
            print(result)
            print("success")
            
            if let dataDict = result as? NSDictionary, let status = dataDict["status"] as? Int, status == 1{
                if let cartArray = dataDict.value(forKey: "cart") as? NSArray {
                    let cartCount = cartArray.count
                    self.cartTotalLabel.text = cartCount.description
                    UserDefaults.standard.set(cartCount.description, forKey: "cartCount")
                }
                self.addToCartButton.isHidden = true;
                for item in 0..<products.count{
                    products[item].count = 0
                }
                self.collectionView.reloadData()
            }
            else{
            }
        }) { (error) in
            AFWrapperClass.alert(Constants.ApplicationName, message: error.localizedDescription, view: self)
    }    
}
    
    @IBAction func tapped_CartButton(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "cartController") as! cartController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    
    func animationFlyToCart () {
        
//      //  print(animationArray)
//      //  print(animationCount)
//
//        let senderButton = animationArray[animationCount] as! UIButton
//        guard let cell = self.collectionView.cellForItem(at: IndexPath.init(item: senderButton.tag, section: 0)) as? MyCollectionViewCell else {
//            return
//        }
//
//
//        let buttonPosition = senderButton.convert(CGPoint(), to:collectionView)
//        let imgViewTemp = UIImageView(frame: CGRect(x: buttonPosition.x, y:  buttonPosition.y, width: 60, height: 60))
//        //imgViewTemp.sd_setImage(with: URL(string: senderButton.imageView?.image), placeholderImage: UIImage(named: "placeholder.png"))
//
//        imgViewTemp.image = cell.productImage.image ?? #imageLiteral(resourceName: "logo")
//        imgViewTemp.layer.cornerRadius = imgViewTemp.frame.height / 2.0
//        imgViewTemp.layer.masksToBounds = true
        
        
      //  self.view.addSubview(imgViewTemp)
        UIView.animate(withDuration: 0.7,
                       animations: {
                     //   imgViewTemp.animationZoom(scaleX: 1.5, y: 1.5)
        }, completion: { _ in
            
            UIView.animate(withDuration: 0.7, animations: {
//                imgViewTemp.animationZoom(scaleX: 0.2, y: 0.2)
//                imgViewTemp.animationRoted(angle: CGFloat(Double.pi))
//                imgViewTemp.frame.origin.x = self.cartView.frame.origin.x + 5
//                imgViewTemp.frame.origin.y = self.cartView.frame.origin.y + 5
                
            }, completion: { _ in
               // imgViewTemp.removeFromSuperview()
                
                UIView.animate(withDuration: 0.7, animations: {
                    self.buttonCart.animationZoom(scaleX: 2.5, y: 2.5)
                  //  self.buttonCart.anima(angle: CGFloat(Double.pi))

                }, completion: {_ in
                    self.buttonCart.animationZoom(scaleX: 1.0, y: 1.0)
//                    if self.animationCount < self.animationArray.count-1 {
//                        self.animationCount += 1
//                        self.animationFlyToCart()
//                    }
                })
            })
        })
    }}
  

extension UIView{
    func animationZoom(scaleX: CGFloat, y: CGFloat) {
        self.transform = CGAffineTransform(scaleX: scaleX, y: y)
    }
    
    func animationRoted(angle : CGFloat) {
        self.transform = self.transform.rotated(by: angle)
    }
}

