//
//  myOrdersController.swift
//  SwiftDemo
//
//  Created by Rakesh Kumar on 01/02/18.
//  Copyright © 2018 Rakesh Kumar. All rights reserved.
//

import UIKit

class tableOrderCell: UITableViewCell{
    @IBOutlet weak var orderNumberLabel: UILabel!
    @IBOutlet weak var orderStatusLabel: UILabel!
    @IBOutlet weak var paidMethodLabel: UILabel!
    @IBOutlet weak var orderedDateLabel: UILabel!
    @IBOutlet weak var totalPriceLabel: UILabel!
    @IBOutlet weak var statusImageView: UIImageView!
}

class myOrdersController: UIViewController,UITableViewDelegate,UITableViewDataSource{
    let cellReuseIdentifier = "OrderCell"
    @IBOutlet weak var ordersTableView: UITableView!
    
    override func viewDidLoad(){
        super.viewDidLoad()
        
        apiGetOrders()
        ordersTableView.delegate = self
        ordersTableView.dataSource = self
        ordersTableView.reloadData()
        self.navigationController?.isNavigationBarHidden = true
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell:tableOrderCell = self.ordersTableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! tableOrderCell!
       if indexPath.row == 0 {
            cell.orderNumberLabel?.text = "OORDER NO. 21641"
            cell.orderStatusLabel?.text = "Packed"
            cell.paidMethodLabel?.text = "Pre Paid"
            cell.totalPriceLabel?.text = "14"
          //  cell.statusImageView?.image = #imageLiteral(resourceName: "bar2")
        }
        else if indexPath.row == 1{
            cell.orderNumberLabel?.text = "OORDER NO. 21642"
            cell.orderStatusLabel?.text = "Placed"
            cell.paidMethodLabel?.text = "Cash On Delivery"
            cell.totalPriceLabel?.text = "15"
          //  cell.statusImageView?.image = #imageLiteral(resourceName: "bar1")
        }
        else{
            cell.orderNumberLabel?.text = "OORDER NO. 21700"
            cell.orderStatusLabel?.text = "Delivered"
            cell.paidMethodLabel?.text = "Cash On Delivery"
            cell.totalPriceLabel?.text = "16"
           // cell.statusImageView?.image = #imageLiteral(resourceName: "bar4")
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      //  let vc = self.storyboard?.instantiateViewController(withIdentifier: "communicationController") as! communicationController
      //  self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 180.0;
    }
    @IBAction func tappedAction_backbutton(_ sender: Any){
        sideMenuVC.toggleMenu()
        }
    
    
    func apiGetOrders() {
        NetworkHelper.sharedInstance.sendAyncronousRequest(method: "user/order", requestType: "GET", params: nil, showHud: true, completion: { (result) in
            if let dataDict = result as? NSDictionary, let status = dataDict["status"] as? Int, status == 1{
                
                print(dataDict)
//                if let userDict = dataDict["transactions"] as? NSDictionary {
//                    let transactionArray = userDict.value(forKey: "docs") as? NSArray
//                    self.arrayOfTransactions = WalletModel().getwalletModel(arr: transactionArray!)
//                    self.transactionTableView.delegate = self
//                    self.transactionTableView.dataSource = self
//                    self.transactionTableView.reloadData()
//                }}
            }
            else{
            }})
        { (error) in
            AFWrapperClass.alert(Constants.ApplicationName, message: error.localizedDescription, view: self)
        }
    }
    
    
    
}


