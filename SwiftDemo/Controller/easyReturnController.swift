//
//  easyReturnController.swift
//  SwiftDemo
//
//  Created by Rakesh Kumar on 15/05/18.
//  Copyright © 2018 Rakesh Kumar. All rights reserved.
//

import UIKit
import MessageUI
import Foundation

class easyReturnCell: UITableViewCell{
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var TitleLabel: UILabel!
    @IBOutlet weak var IconImageView: UIImageView!
}

class easyReturnController: UIViewController,UITableViewDelegate,UITableViewDataSource,MFMailComposeViewControllerDelegate{
    
    @IBOutlet weak var tableView: UITableView!
    
    let descriptions = ["We promote paperless billing. Help us in keeping the city clean and save nature - Keep your email address updated for invoices.", "+919873010700", "Grocery items can be replaced within 2 days of delivery.", "support@groccy.com"]
    
     let titleNamw = ["E-Billing", "WhatsApp", "Guaranteed Freshness", "Contact Us"]
    
    let imageIcon = [#imageLiteral(resourceName: "icn_inner_ebilling"), #imageLiteral(resourceName: "icn_inner_whatsapp"), #imageLiteral(resourceName: "icn_inner_freshness"), #imageLiteral(resourceName: "icn_inner_contact")]
  
    // MARK:  View Setup
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.reloadData()
    }

    @IBAction func tappedAction_backButton(_ sender: Any) {
        if let navController = self.navigationController {
            navController.popViewController(animated: true)
        }
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell:easyReturnCell = self.tableView.dequeueReusableCell(withIdentifier: "easyReturnCell") as! easyReturnCell!
        cell.TitleLabel.text =  self.titleNamw[indexPath.row]
        cell.descriptionLabel.text = self.descriptions[indexPath.row]
        cell.IconImageView.image = imageIcon[indexPath.row]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row == 1{
            let msg = "Hello! I have a question"
            let urlWhats = "whatsapp://send?phone=+919592006789&text=\(msg)"
            if let urlString = urlWhats.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed){
                if let whatsappURL = NSURL(string: urlString) {
                    if UIApplication.shared.canOpenURL(whatsappURL as URL) {
                        if #available(iOS 10.0, *) {
                            _ = UIApplication.shared.open(whatsappURL as URL, options: [:], completionHandler: nil)
                        } else {
                            // Fallback on earlier versions
                        }
                    } else {
                        // Cannot open whatsapp
                     //   ViewUtil().popAlertView(vc, title: "WhatsApp Support", message: "Please WhatsApp us at +919592006789 if you have any questions", option: "Ok")
                    }
                }
            }
        }
        else if indexPath.row == 3 {
            if !MFMailComposeViewController.canSendMail() {
                print("Mail services are not available")
                return
            }
            sendEmail()
        }
        
        //  let vc = self.storyboard?.instantiateViewController(withIdentifier: "communicationController") as! communicationController
        //  self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func sendEmail() {
        let composeVC = MFMailComposeViewController()
        composeVC.mailComposeDelegate = self
        // Configure the fields of the interface.
        composeVC.setToRecipients(["support@groccy.com"])
        composeVC.setSubject("I want Groccy too")
        composeVC.setMessageBody("Tell us your name, city, area and phone number. We will be there for you! \n\n Love, Team Groccy.", isHTML: false)
        // Present the view controller modally.
        self.present(composeVC, animated: true, completion: nil)
    }
    
    
    func mailComposeController(_ controller: MFMailComposeViewController,
                               didFinishWith result: MFMailComposeResult, error: Error?) {
        // Check the result or perform other tasks.
        
        // Dismiss the mail compose view controller.
        controller.dismiss(animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 120;
    }
}

