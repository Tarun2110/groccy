//
//  ViewController.m
//  PaymentGateway
//
//  Created by Suraj on 22/07/15.
//  Copyright (c) 2015 Suraj. All rights reserved.
//

#import "PaymentPageViewController.h"
#import <CommonCrypto/CommonDigest.h>

@interface PaymentPageViewController () <UIWebViewDelegate, UIAlertViewDelegate> {
    UIActivityIndicatorView *activityIndicatorView;
    NSString *strMIHPayID;
    NSDictionary *paymentDic;
}


@property (weak, nonatomic) IBOutlet UIWebView *webviewPage;

//@property(nonatomic, strong) AppDelegate* appDel;

@end

@implementation PaymentPageViewController

@synthesize name,amount,txnId,email,phone,rechargeId,recharge, orderId;

//#define Base_URL @"https://secure.payu.in"
//#define Merchant_Key @"cjrBMu"
//#define Salt @"GJxpjXoq"
//
// Testing key, salt and url
#define Merchant_Key @"9HwJ5t"
#define Salt @"2JIfn3ez"
#define Base_URL @"https://test.payu.in"


//#define Merchant_Key @"9HwJ5t"
//#define Salt @"2JIfn3ez"
//#define Base_URL @"https://test.payu.in/"

#define Success_URL @"http://app.carsake.com/App/HondaApp/OmHonda/paysucess.php"
#define Failure_URL @"http://app.carsake.com/App/HondaApp/OmHonda/payfailure.php"
#define Product_Info @"Bike Booking"
#define Paid_Amount @"1.00"
#define Payee_Name @"Chandan Kumar"

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    [self setTitle:@"Make A Payment"];
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:11.0 / 255 green:22.0 / 255 blue:33.0 / 255 alpha:1];
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackTranslucent;
    self.navigationController.navigationBar.titleTextAttributes = @{
                                                                    NSFontAttributeName: [UIFont boldSystemFontOfSize:17.0],
                                                                    NSForegroundColorAttributeName: [UIColor whiteColor]
                                                                    };
    
    [self initPayment];
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:YES];
    [self setTitle:@""];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
  //  [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    
    self.webviewPage.delegate=self;
    activityIndicatorView = [[UIActivityIndicatorView alloc] init];
    activityIndicatorView.center = self.view.center;
    [activityIndicatorView setColor:[UIColor blackColor]];
    [self.view addSubview:activityIndicatorView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)initPayment {

    NSString *successUrl = [[NSUserDefaults standardUserDefaults] valueForKey:@"successUrl"];
    NSString *failUrl = [[NSUserDefaults standardUserDefaults] valueForKey:@"failureUrl"];

    
    
    int i = arc4random() % 9999999999;
    NSString *strHash = [self createSHA512:[NSString stringWithFormat:@"%d%@",i,[NSDate date]]];// Generatehash512(rnd.ToString() + DateTime.Now);
    NSString *txnid1 = [strHash substringToIndex:20];
    strMIHPayID = txnid1;
    NSString *key = Merchant_Key;
    NSString *amount1 = amount;
    NSString *productInfo = _productInfo;
    NSString *firstname = name;
    NSString *email1 = email; // Generated a fake mail id for testing
    NSString *phone1 = phone;
    NSString *serviceprovider = @"payu_paisa";
    NSString *order = orderId;
    NSDictionary *parameters;
    
    
    NSString *hashValue = [NSString stringWithFormat:@"%@|%@|%@|%@|%@|%@|||||||||||%@",key,txnid1,amount1,productInfo,firstname,email1,Salt];
    NSString *hash = [self createSHA512:hashValue];
    
    parameters = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:order, txnid1,key,amount1,productInfo,firstname,email1,phone1,successUrl,failUrl,hash,serviceprovider
                                                      , nil] forKeys:[NSArray arrayWithObjects:@"order_id",@"txnid",@"key",@"amount",@"productinfo",@"firstname",@"email",@"phone",@"surl",@"furl",@"hash",@"service_provider", nil]];
    __block NSString *post = @"";
    [parameters enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
        if ([post isEqualToString:@""]) {
            post = [NSString stringWithFormat:@"%@=%@",key,obj];
        } else {
            post = [NSString stringWithFormat:@"%@&%@=%@",post,key,obj];
        }
    }];
    
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    
    NSString *postLength = [NSString stringWithFormat:@"%lu",(unsigned long)[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/_payment",Base_URL]]];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Current-Type"];
    [request setHTTPBody:postData];
    
    [_webviewPage loadRequest:request];
    [activityIndicatorView startAnimating];
}

-(NSString *)createSHA512:(NSString *)string {
    const char *cstr = [string cStringUsingEncoding:NSUTF8StringEncoding];
    NSData *data = [NSData dataWithBytes:cstr length:string.length];
    uint8_t digest[CC_SHA512_DIGEST_LENGTH];
    CC_SHA512(data.bytes, (CC_LONG)data.length, digest);
    NSMutableString* output = [NSMutableString  stringWithCapacity:CC_SHA512_DIGEST_LENGTH * 2];
    for(int i = 0; i < CC_SHA512_DIGEST_LENGTH; i++) {
        [output appendFormat:@"%02x", digest[i]];
    }
    return output;
}

#pragma UIWebView - Delegate Methods
-(void)webViewDidStartLoad:(UIWebView *)webView {
    [activityIndicatorView startAnimating];
    
}

-(void)webViewDidFinishLoad:(UIWebView *)webView {
    [activityIndicatorView stopAnimating];
    
    if (webView.isLoading) {
        return;
    }
    NSURL *requestURL = [[_webviewPage request] URL];
    NSString *getStringFromUrl = [NSString stringWithFormat:@"%@",requestURL];
    
    if ([self containsString:getStringFromUrl :Success_URL])
    {
        //[SVProgressHUD showWithStatus:@"Loading..."];
        //[self performSelector:@selector(delayedDidFinish:) withObject:getStringFromUrl afterDelay:0.0];
    }
    else if ([self containsString:getStringFromUrl :Failure_URL]) {
        // FAILURE ALERT
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Sorry !!!" message:@"Your transaction failed. Please order again!" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        alert.tag = 1;
        [alert show];
    }
}

-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    [activityIndicatorView stopAnimating];
    NSURL *requestURL = [[_webviewPage request] URL];
    if (error.code == -1009 || error.code == -1003 || error.code == -1001) { //error.code == -999
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Oops !!!" message:@"Please check your internet connection!" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        alert.tag = 1;
        [alert show];
    }
}

- (void)delayedDidFinish:(NSString *)getStringFromUrl {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        [self postsuccessorder];
        
    });
}

#pragma UIAlertView - Delegate Method
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (alertView.tag == 1 && buttonIndex == 0) {
        // Navigate to Payment Status Screen
        NSMutableDictionary *mutDictTransactionDetails = [[NSMutableDictionary alloc] init];
        [self navigateToPaymentStatusScreen:mutDictTransactionDetails];
    }
}

- (BOOL)containsString: (NSString *)string : (NSString*)substring {
    return [string rangeOfString:substring].location != NSNotFound;
}

- (void)navigateToPaymentStatusScreen: (NSMutableDictionary *)mutDictTransactionDetails {
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [self.navigationController popViewControllerAnimated:YES];
        
    });
}

-(void)postsuccessorder
{
    NSString *myRequestString = [[NSString alloc] initWithFormat:@"lang=%@&value=%@", @"lang", @"0"];
    NSData *hsRequestData = [ NSData dataWithBytes: [ myRequestString UTF8String ] length: [ myRequestString length ] ];
    NSString *str = [NSString stringWithFormat:@"http://spectsbazaarapi.buildmeaplace.com/api/SpectsProduct/UpdateOrder?TxnID=%@&OrderID=%@&PaidAmount=%@",strMIHPayID,orderId,amount];
    NSString * encodedString = [str stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
    NSMutableURLRequest *request = [ [ NSMutableURLRequest alloc ] initWithURL: [ NSURL URLWithString:encodedString]];
    [request setHTTPMethod: @"PUT"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    [request setHTTPBody: hsRequestData];
    NSURLSessionTask *task = [[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        paymentDic = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
        [self performSelectorOnMainThread:@selector(mainMethod) withObject:nil waitUntilDone:NO];
        
    }];
    [task resume];
   
}
-(void)mainMethod
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Payment Success" message:nil preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alertController addAction:ok];
    [self presentViewController:alertController animated:YES completion:nil];
  //  HomeViewController *VC=[self.storyboard instantiateViewControllerWithIdentifier:@"HomeViewController"];
   // [self.navigationController pushViewController:VC animated:YES];
    [UIView beginAnimations:@"Custom" context:nil];
    [UIView setAnimationDuration:1.0f];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromRight forView:self.navigationController.view cache:NO];
    [UIView commitAnimations];
}

- (IBAction)backAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
//- (BOOL)connected
//{
//    return [AFNetworkReachabilityManager sharedManager].reachable;
//}
@end
