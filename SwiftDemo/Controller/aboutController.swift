//
//  aboutController.swift
//  SwiftDemo
//
//  Created by Rakesh Kumar on 31/01/18.
//  Copyright © 2018 Rakesh Kumar. All rights reserved.
//

import UIKit

class aboutController: UIViewController{
    
    @IBOutlet weak var aboutUsTextView: UITextView!
   
    
    override func viewDidLoad(){
        super.viewDidLoad()
        let aboutUsString =  UserDefaults.standard.string(forKey: "aboutUs")
        if let htmlData = aboutUsString?.data(using: String.Encoding.unicode) {
            do {
                let attributedText =  try NSAttributedString(data: htmlData,
                                              options: [.documentType: NSAttributedString.DocumentType.html,
                                                        .characterEncoding: String.Encoding.utf8.rawValue],
                                              documentAttributes: nil)
                aboutUsTextView.attributedText = attributedText
            } catch _ as NSError {
            }
        }
        self.navigationController?.isNavigationBarHidden = true
    }

    @IBAction func tappeAction_menubutton(_ sender: Any) {
        sideMenuVC.toggleMenu()
    }
    

}
