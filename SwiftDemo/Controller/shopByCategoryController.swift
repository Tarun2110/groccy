//
//  shopByCategoryController.swift
//  SwiftDemo
//
//  Created by Rakesh Kumar on 15/05/18.
//  Copyright © 2018 Rakesh Kumar. All rights reserved.
//

import UIKit
import Foundation
class CategoryNameCVCell: UICollectionViewCell {
    @IBOutlet weak var nameLbl: UILabel!
    
}

class CategoryExpandCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    @IBOutlet weak var productsCollection: UICollectionView!
    var subCategories = [subCategoryModel]()
    func setUpell(catSubCategories:[subCategoryModel]) {
        subCategories = catSubCategories
        productsCollection.delegate = self
        productsCollection.dataSource = self
        productsCollection.reloadData()
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return subCategories.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryNameCVCell", for: indexPath as IndexPath) as! CategoryNameCVCell
        cell.nameLbl.text = subCategories[indexPath.item].sCategoryName
        cell.nameLbl.font = UIFont(name:"Montserrat-Regular", size: 13.0)
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let str = subCategories[indexPath.item].sCategoryName
        let width = str.width(withConstraintedHeight: 40, font: UIFont.systemFont(ofSize: 30))
        return CGSize(width: width - 10, height: 40);
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 2.0
    }
    
}

class CategoryTVCell: UITableViewCell {
    @IBOutlet weak var actionButton: UIButton!
    @IBOutlet weak var expandImg: UIImageView!
    @IBOutlet weak var categoryNameLbl: UILabel!
}

class shopByCategoryController: UIViewController {
    
    var appDel = AppDelegate()
    var selectedCategory = -1
    @IBOutlet weak var categoryTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        appDel = UIApplication.shared.delegate as! AppDelegate
    }
    
    @IBAction func menuButtonTap(_ sender: Any) {
        sideMenuVC.toggleMenu()
    }
    
    @objc func actionButtonTap(_ sender: UIButton) {
        selectedCategory = (selectedCategory == sender.tag) ? -1 : sender.tag
        categoryTableView.reloadData()
    }
}


extension shopByCategoryController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (selectedCategory == section) ? 1 : 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:CategoryExpandCell = self.categoryTableView.dequeueReusableCell(withIdentifier: "CategoryExpandCell") as! CategoryExpandCell!
        cell.setUpell(catSubCategories: appDel.arrayOfCategoreies[indexPath.section].catSubCategories)
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return appDel.arrayOfCategoreies.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell:CategoryTVCell = self.categoryTableView.dequeueReusableCell(withIdentifier: "CategoryTVCell") as! CategoryTVCell!
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        cell.categoryNameLbl.text = appDel.arrayOfCategoreies[section].catName
        cell.categoryNameLbl.font = UIFont(name:"Montserrat-Regular", size: 13.0)
        cell.expandImg.image = (selectedCategory == section) ? #imageLiteral(resourceName: "minus") : #imageLiteral(resourceName: "add")
        cell.actionButton.tag = section
        cell.actionButton.addTarget(self, action: #selector(self.actionButtonTap(_:)), for: .touchUpInside)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 45
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    
}
