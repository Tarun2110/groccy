//
//  groccyAccountController.swift
//  SwiftDemo
//
//  Created by Rakesh Kumar on 20/02/18.
//  Copyright © 2018 Rakesh Kumar. All rights reserved.
//
import UIKit
import Alamofire

class groccyAccountController: UIViewController,UINavigationControllerDelegate,UIPickerViewDataSource,UIPickerViewDelegate,UIImagePickerControllerDelegate,UITextFieldDelegate{
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var phoneNumberLabel: UILabel!
    
    @IBOutlet weak var menuButton: UIButton!
    
    @IBOutlet weak var nameTextfield: UITextField!
    @IBOutlet weak var phoneTextfield: UITextField!
    @IBOutlet weak var cityTextfield: UITextField!
    @IBOutlet weak var areaTextField: UITextField!
    @IBOutlet weak var emailTextfield: UITextField!
    
    @IBOutlet weak var mainOptionsView: UIView!
    @IBOutlet weak var proflieFormView: UIView!
    var isclicked = false

    @IBOutlet weak var profilePic: UIImageView!
    
    var cityPlace = 0
    var selectedCity = ""
    var selectedArea = ""
    var fileName = ""

    var cityArray = [AreaModel]()
    var sectorsArray = [SectorModel]()
    let pickerView = UIPickerView()
    var pickerController = UIImagePickerController()
    
    var userProfileImage: UIImage?
    
    override func viewDidLoad(){
        super.viewDidLoad()
        isclicked  = false

        pickerView.delegate = self
        cityTextfield.inputView = pickerView
        areaTextField.inputView = pickerView

        self.proflieFormView.alpha = 0.0
        self.mainOptionsView.alpha = 1.0

        cityTextfield.delegate = self
        cityTextfield.tag = 100
        
        areaTextField.delegate = self
        areaTextField.tag = 101
        
        let paddingView : UIView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 30))
        let paddingViewR : UIView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 30))
        
        let imageView = UIImageView(frame: CGRect(x: -10, y: 12.5, width: 10, height: 5))
        let image = UIImage(named: "arrow_down")
        imageView.image = image
        imageView.contentMode = .center
        
        paddingViewR.addSubview(imageView)
        cityTextfield.rightView = paddingViewR
        cityTextfield.leftView = paddingView
        cityTextfield.leftViewMode = .always
        cityTextfield.rightViewMode = .always
        
        
        let paddingLeft : UIView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 30))
        let paddingRight : UIView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 30))
        
        let imageV = UIImageView(frame: CGRect(x: -10, y: 12.5, width: 10, height: 5))
        let imageVa = UIImage(named: "arrow_down")
        imageV.image = imageVa
        imageV.contentMode = .center
        
        paddingRight.addSubview(imageV)
        areaTextField.rightView = paddingRight
        areaTextField.leftView = paddingLeft
        areaTextField.leftViewMode = .always
        areaTextField.rightViewMode = .always

        self.navigationController?.isNavigationBarHidden = true
        nameTextfield.isUserInteractionEnabled = true
        phoneTextfield.isUserInteractionEnabled = true
        cityTextfield.isUserInteractionEnabled = true
        areaTextField.isUserInteractionEnabled = true
        
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapBlurButton(_:)))
        profilePic.addGestureRecognizer(tapGesture)
        
        
        self.apiGetUserProfile()
    }
    
    @IBAction func tappedAction_changePhone(_ sender: Any) {
      
        UserDefaults.standard.set(false, forKey: "fromAlert")

        let vc = self.storyboard?.instantiateViewController(withIdentifier: "alertController") as! alertController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func tapBlurButton(_ sender: UITapGestureRecognizer) {
        if isclicked == true{
            let alertViewController = UIAlertController(title: "", message: "Choose your option", preferredStyle: .actionSheet)
            let camera = UIAlertAction(title: "Camera", style: .default, handler: { (alert) in
                self.openCamera()
            })
            let gallery = UIAlertAction(title: "Gallery", style: .default) { (alert) in
                self.openGallary()
            }
            let cancel = UIAlertAction(title: "Cancel", style: .cancel) { (alert) in
                
            }
            alertViewController.addAction(camera)
            alertViewController.addAction(gallery)
            alertViewController.addAction(cancel)
            self.present(alertViewController, animated: true, completion: nil)
        }    }
    
    
    func openCamera() {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
            pickerController.delegate = self 
            self.pickerController.sourceType = UIImagePickerControllerSourceType.camera
            pickerController.allowsEditing = true
            self .present(self.pickerController, animated: true, completion: nil)
        }
        else {
            let alertWarning = UIAlertView(title:"Warning", message: "You don't have camera", delegate:nil, cancelButtonTitle:"OK", otherButtonTitles:"")
            alertWarning.show()
        }
    }
    func openGallary() {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
            pickerController.delegate = self
            pickerController.sourceType = UIImagePickerControllerSourceType.photoLibrary
            pickerController.allowsEditing = true
            self.present(pickerController, animated: true, completion: nil)
        }
    }
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            
            if #available(iOS 11.0, *) {
                guard let fileUrl = info[UIImagePickerControllerImageURL] as? URL else { return }
                print(fileUrl.lastPathComponent)

                
                fileName = fileUrl.lastPathComponent
            } else {
                // Fallback on earlier versions
            }
            
            
            
            self.userProfileImage = image
            self.profilePic.contentMode = .scaleAspectFill
            self.profilePic.image = image
            picker.dismiss(animated: true, completion: nil)
        }
        
       // self.uploadImages2(images: image)

    }
  
    public func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        print("Cancel")
    }
    
    @IBAction func tappedAction_profileButton(_ sender: Any) {
        isclicked = true

        UIView.animate(withDuration: 0.5,
                       delay: 0.1,
                       options: UIViewAnimationOptions.transitionCurlUp,
                       animations: { () -> Void in
                        self.proflieFormView.alpha = 1.0
                        self.mainOptionsView.alpha = 0.0
                        self.menuButton.setImage(UIImage(named: "icn_backWhite"), for: .normal)
        }, completion: { (finished) -> Void in
        })
    }
    
    
    @IBAction func tappedAction_menubutton(_ sender: Any) {
        print( isclicked)
        
        if isclicked == true {
            
            UIView.animate(withDuration: 0.5,
                           delay: 0.1,
                           options: UIViewAnimationOptions.transitionCurlUp,
                           animations: { () -> Void in
                            self.proflieFormView.alpha = 0.0
                            self.mainOptionsView.alpha = 1.0
                            self.menuButton.setImage(UIImage(named: "icn_nav_menu"), for: .normal)
            }, completion: { (finished) -> Void in
            })
            isclicked = false
        }else{
            //isclicked = true
            sideMenuVC.toggleMenu()
            menuButton.setImage(UIImage(named: "icn_nav_menu"), for: .normal)
        }
    }

    @IBAction func tappedAction_alertPreference(_ sender: Any){
        UserDefaults.standard.set(true, forKey: "fromAlert")
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "alertController") as! alertController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    

    @IBAction func tappedAction_Edit(_ sender: Any) {
//      self.apiUpdateUserProfile()
        guard let userName = nameTextfield.text, userName != "" else {
            return
        }
        guard let email = emailTextfield.text, email != "" else {
            return
        }
        self.updateUserProfile(userName: userName, email: email, city: selectedCity, area: selectedArea)
    }
    
    @IBAction func tappedAction_newAddress(_ sender: Any){
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "addressesController") as! addressesController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func tappedAction_wallet(_ sender: Any){
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "myWalletController") as! myWalletController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func apiGetUserProfile() {
        NetworkHelper.sharedInstance.sendAyncronousRequest(method: "user/profile", requestType: "GET", params: nil, showHud: true, completion: { (result) in
            if let dataDict = result as? NSDictionary, let status = dataDict["status"] as? Int, status == 1{
                if let userDict = dataDict["user"] as? NSDictionary {
                    self.nameTextfield.text = userDict["name"] as? String
                    self.emailTextfield.text = userDict.object(forKey: "email") as? String
                    self.phoneTextfield.text = userDict.object(forKey: "phone") as? String
                    UserDefaults.standard.set(self.phoneTextfield.text, forKey: "registeredNumber")

                    self.profilePic.sd_setImage(with: URL(string: (userDict["profile_picture"] as? String)!), placeholderImage: UIImage(named: "profile_picture"))
                    self.profilePic.contentMode = .scaleAspectFill

                    self.nameLabel.text  = userDict["name"] as? String
                    let alertNumber = userDict["alertPreferenceNo"] as? String
                    UserDefaults.standard.set(alertNumber, forKey: "alertNumber")
                    
                    self.phoneNumberLabel.text = userDict.object(forKey: "phone") as? String
                    if let cityDict = userDict["city"] as? NSDictionary, let cityName = cityDict["name"] as? String{
                        self.cityTextfield.text = cityName
                        self.selectedCity = (cityDict["_id"] as? String)!
                    }
                    if let areaDict = userDict["area"] as? NSDictionary, let areaName = areaDict["name"] as? String{
                        self.areaTextField.text = areaName
                        self.selectedArea = (areaDict["_id"] as? String)!
                    }
                }}
            else{
            }})
        { (error) in
            AFWrapperClass.alert(Constants.ApplicationName, message: error.localizedDescription, view: self)
        }
    }
    

    func apiGetAllCities() {
        NetworkHelper.sharedInstance.sendAyncronousRequest(method: "city", requestType: "GET", params: nil, showHud: false, completion: { (result) in
            print(result)
            if let dataDict = result as? NSDictionary, let status = dataDict["status"] as? Int, status == 1, let cityArray = dataDict.value(forKey: "cities") as? NSArray{
                print(cityArray)
                for data in cityArray {
                    if let dic  = data as? NSDictionary {
                        var _id = ""
                        if let areaId = dic["_id"] as? String {
                            _id = areaId
                        }
                        var name = ""
                        if let areaName = dic["name"] as? String {
                            name = areaName
                        }
                        self.cityArray.append(AreaModel.init(_id: _id, name: name))
                        self.pickerView.reloadAllComponents();
                    }}}
            else{
            }
        }) { (error) in
            AFWrapperClass.alert(Constants.ApplicationName, message: error.localizedDescription, view: self)
        }
    }
    
    // MARK:  Picker Delegate
    func numberOfComponents(in pickerView: UIPickerView) -> Int{
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        if cityPlace == 1{
            return self.cityArray.count
        }else if cityPlace == 2{
            return self.sectorsArray.count
        }
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?{
      if cityPlace == 1{
            selectedCity = cityArray[row]._id
            return cityArray[row].name
        }
      else if cityPlace == 2{
        selectedArea = sectorsArray[row].SectorId
        return self.sectorsArray[row].SectorName
        
        }
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int){
    if cityPlace == 1{
        cityTextfield.text = cityArray[row].name
        cityTextfield.textColor = UIColor.black
        }
        else if cityPlace == 2{
        areaTextField.text = sectorsArray[row].SectorName
        areaTextField.textColor = UIColor.black
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.tag == 100 {
            print("chaleya")
            cityPlace = 1
            self.apiGetAllCities()

        }
        else if textField.tag == 101{
            cityPlace = 2
            self.apiGetAllArea()
        }
    }
    
    func updateUserProfile(userName: String, email: String, city: String, area: String) {
        AFWrapperClass.svprogressHudShow(title: "Loading...", view: self)
        let headers : [String : String]?
        if let accessToken =  UserDefaults.standard.string(forKey: "accessToken"), accessToken != "" {
            headers = ["x-access-token" : accessToken,
                       "Content-type" : "multipart/form-data"]
        } else {
            headers = ["Content-type" : "multipart/form-data"]
        }
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            let parameters = ["email": email,
                              "name": userName,
                              "city":city,
                              "area":area]
            for (key, value) in parameters {
                multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
            }
            if let image = self.userProfileImage {
                let imageData = UIImageJPEGRepresentation(image, 0.5)!
                multipartFormData.append(imageData, withName: "profilePic", fileName: self.fileName, mimeType: "image/jpeg")
            }
        }, to:"http://18.218.188.114/api/user/profile",
            headers: headers,
            encodingCompletion: { encodingResult in
                switch encodingResult {
                case .success(let upload, _,_):
                upload.responseJSON { response in
                debugPrint(response)
                    
                DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(3)) {
                        if response.result.isSuccess {
                            AFWrapperClass.svprogressHudDismiss(view: self)
                            self.nameLabel.text = self.nameTextfield.text
                            AFWrapperClass.alert(Constants.ApplicationName, message: "User has been updated successfully!", view: self)
                        } else {
                            AFWrapperClass.svprogressHudDismiss(view: self)
                            let error : NSError = response.result.error! as NSError
                        }                    }
                
                }
                upload.uploadProgress(closure: {
                progress in
                print(progress.fractionCompleted)
                })
                case .failure(let encodingError):
                    print(encodingError)
                    AFWrapperClass.svprogressHudDismiss(view: self)
                    
                }
        })
    }
    


    func apiGetAllArea() {
        let idString = "city/" + selectedCity + "/areas"
        NetworkHelper.sharedInstance.sendAyncronousRequest(method: idString , requestType: "GET", params: nil, showHud: false, completion: { (result) in
            print(result)
            if let dataDict = result as? NSDictionary, let status = dataDict["status"] as? Int, status == 1, let cityArray = dataDict.value(forKey: "areas") as? NSArray{
                self.sectorsArray = SectorModel().getSectorModel(arr: cityArray)
                self.pickerView.reloadAllComponents();
            }
            else{
            }
        }) { (error) in
            AFWrapperClass.alert(Constants.ApplicationName, message: error.localizedDescription, view: self)
        }
    }
}


