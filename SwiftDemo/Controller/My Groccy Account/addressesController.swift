//
//  addressesController.swift
//  SwiftDemo
//
//  Created by Rakesh Kumar on 31/01/18.
//  Copyright © 2018 Rakesh Kumar. All rights reserved.
//

import UIKit


struct AddressModel {
    var lineOne = ""
    var lineTwo = ""
    var AddressName = ""
    var _id = ""
    var primary = ""

    func getAddressModel(arr: NSArray) -> [AddressModel] {
        var Addresses = [AddressModel]()
        for data in arr {
            if let dict = data as? NSDictionary {
                var area = AddressModel()
                if let lineOne = dict["line1"] as? String {
                    area.lineOne = lineOne
                }
                if let lineTwo = dict["line2"] as? String {
                    area.lineTwo = lineTwo
                }
                if let AddressName = dict["name"] as? String {
                    area.AddressName = AddressName
                }
                if let addressId = dict["_id"] as? String {
                    area._id = addressId
                }
                
                if let primaryAddress = dict["primary"] as? Int {
                    area.primary = primaryAddress.description
                }
                Addresses.append(area)
            }}
        return Addresses
    }
}

class tableAddressCell: UITableViewCell {
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var headingLabel: UILabel!
    @IBOutlet weak var editbutton: UIButton!
    @IBOutlet weak var deletebutton: UIButton!
}



class addressesController: UIViewController,UITableViewDataSource,UITableViewDelegate{
    var arrayOfAddresses = [AddressModel]()
    let cellReuseIdentifier = "AddressCell"
  
    @IBOutlet weak var addressTableView: UITableView!
    override func viewDidLoad(){
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
         self.apiGetAddresses()
    }
    
    @IBAction func tappedAction_addNewAddress(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "newAddressController") as! newAddressController
        self.navigationController?.pushViewController(vc, animated: true)
    }
 
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrayOfAddresses.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:tableAddressCell = self.addressTableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! tableAddressCell!
            cell.headingLabel?.text = arrayOfAddresses[indexPath.row].AddressName
            cell.addressLabel?.text = arrayOfAddresses[indexPath.row].lineOne
        
            cell.deletebutton.tag = indexPath.row
            cell.editbutton.tag = indexPath.row
            cell.deletebutton.addTarget(self, action: #selector(self.deletebuttonClicked(_:)), for:.touchUpInside)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    
    @IBAction func tappedAction_backButton(_ sender: Any) {
        let _ = self.navigationController?.popViewController(animated: true)
    }
    
    
    @objc func deletebuttonClicked(_ sender:UIButton){
        let addressId = self.arrayOfAddresses[sender.tag]._id.description
        self.apiDeleteAddress(addressId: addressId)
    }
    
    
    func apiDeleteAddress (addressId:String) {
      
        let methodString = "user/address/" + addressId
        NetworkHelper.sharedInstance.sendAyncronousRequest(method: methodString , requestType: "DELETE", params: nil, showHud: true, completion: { (result) in
            print(result)
            if let dataDict = result as? NSDictionary, let status = dataDict["status"] as? Int, status == 1{
                AFWrapperClass.alert(Constants.ApplicationName, message: "Address has been deleted successfully!", view: self)
                self.apiGetAddresses()
            }
            else{
                if let dataDict = result as? NSDictionary, let status = dataDict["message"] as? String{
                    AFWrapperClass.alert(Constants.ApplicationName, message: status, view: self)
                }
            }
            self.apiGetAddresses()
            
        }) { (error) in
            AFWrapperClass.alert(Constants.ApplicationName, message: "Something went wrong.", view: self)
        }
    }
    
    
    func apiGetAddresses() {
        NetworkHelper.sharedInstance.sendAyncronousRequest(method: "user/addresses", requestType: "GET", params: nil, showHud: false, completion: { (result) in
            print(result)
            print("success")
            if let dataDict = result as? NSDictionary, let status = dataDict["status"] as? Int, status == 1{
                let addressesArray = dataDict.value(forKey: "addresses") as? NSArray
            if addressesArray?.count != 0{
                self.arrayOfAddresses = AddressModel().getAddressModel(arr: addressesArray!)
                print(self.arrayOfAddresses)
                self.addressTableView.delegate = self
                self.addressTableView.dataSource = self
                self.addressTableView.reloadData()
                }
                else{
                    AFWrapperClass.alert(Constants.ApplicationName, message: "Please add a New Address", view: self)
                }
            }
            else{
            }
        }) { (error) in
            AFWrapperClass.alert(Constants.ApplicationName, message: error.localizedDescription, view: self)
        }
    }
    
    
}
