 //
//  myWalletController.swift
//  SwiftDemo
//
//  Created by Rakesh Kumar on 30/01/18.
//  Copyright © 2018 Rakesh Kumar. All rights reserved.
 
 import UIKit

 struct WalletModel {
    var __v = ""
    var _id = ""
    var amount = 0
    var created_at = ""
    var description = ""
    var expired_at = ""
    var user = ""
    var wallet_type = ""

    
    func getwalletModel(arr: NSArray) -> [WalletModel] {
        var walletAddress = [WalletModel]()
        for data in arr {
            if let dict = data as? NSDictionary {
                var walletObject = WalletModel()
                if let BId = dict["_id"] as? String {
                    walletObject._id = BId
                }
                if let amountTransfer = dict["amount"] as? Int {
                    
                    print(amountTransfer)
                    walletObject.amount = amountTransfer
                }
                if let description = dict["description"] as? String {
                    walletObject.description = description
                }
                if let createdDate = dict["created_at"] as? String {
                    walletObject.created_at = createdDate
                }
                if let expiry = dict["expired_at"] as? String {
                    walletObject.expired_at = expiry
                }
                if let user = dict["user"] as? String {
                    walletObject.user = user
                }
                if let wallet_type = dict["wallet_type"] as? String {
                    walletObject.wallet_type = wallet_type
                }
                walletAddress.append(walletObject)
            }}
        return walletAddress
    }
 }
 
 
 class tableCouponCell: UITableViewCell{
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var orderNumberLabel: UILabel!
    @IBOutlet weak var orderDateLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var walletImage: UIImageView!
    
 }
 
 class myWalletController: UIViewController,UITableViewDelegate,UITableViewDataSource{
    @IBOutlet weak var transactionTableView: UITableView!
    @IBOutlet weak var walletAmountText: UILabel!

    var  arrayOfTransactions = [WalletModel]()

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
   
    override func viewDidLoad(){
        super.viewDidLoad()
       
        apiGetUserWallet()
        self.navigationController?.isNavigationBarHidden = true
        let walletString =  UserDefaults.standard.object(forKey: "walletAmount") as? Int
        self.walletAmountText.text = "Rs." + (walletString?.description)!
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrayOfTransactions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell:tableCouponCell = self.transactionTableView.dequeueReusableCell(withIdentifier: "CouponCell") as! tableCouponCell!
        
        let transactiontype = self.arrayOfTransactions[indexPath.row].wallet_type
        if transactiontype == "credit" {
            cell.walletImage.image = #imageLiteral(resourceName: "wallet_added")
            cell.amountLabel.text =  "+ Rs." + self.arrayOfTransactions[indexPath.row].amount.description
        }
        else{
            cell.walletImage.image = #imageLiteral(resourceName: "wallet_paid")
            cell.amountLabel.text =  "- Rs." + self.arrayOfTransactions[indexPath.row].amount.description
        }
         cell.titleLabel?.text = self.arrayOfTransactions[indexPath.row].description.uppercased()
        cell.orderDateLabel.text = self.arrayOfTransactions[indexPath.row].created_at
        cell.orderNumberLabel.text = "Order ID - " + self.arrayOfTransactions[indexPath.row]._id
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      //  let vc = self.storyboard?.instantiateViewController(withIdentifier: "communicationController") as! communicationController
       // self.navigationController?.pushViewController(vc, animated: true)
    }
   
    @IBAction func tappedAction_backbutton(_ sender: Any){
        let _ = self.navigationController?.popViewController(animated: true)
    }
    
    func apiGetUserWallet() {
        NetworkHelper.sharedInstance.sendAyncronousRequest(method: "user/wallet", requestType: "GET", params: nil, showHud: true, completion: { (result) in
            if let dataDict = result as? NSDictionary, let status = dataDict["status"] as? Int, status == 1{
                if let userDict = dataDict["transactions"] as? NSDictionary {
                     let transactionArray = userDict.value(forKey: "docs") as? NSArray
                    self.arrayOfTransactions = WalletModel().getwalletModel(arr: transactionArray!)
                    self.transactionTableView.delegate = self
                    self.transactionTableView.dataSource = self
                    self.transactionTableView.reloadData()
                }}
            else{
            }})
        { (error) in
            AFWrapperClass.alert(Constants.ApplicationName, message: error.localizedDescription, view: self)
        }
    }
    
}

