//
//  alertController.swift
//  SwiftDemo
//
//  Created by Rakesh Kumar on 24/05/18.
//  Copyright © 2018 Rakesh Kumar. All rights reserved.
//

import UIKit
import Foundation

class alertController: UIViewController {
    @IBOutlet weak var phone_textfield: UITextField!
    @IBOutlet weak var pincode_textfield: PinCodeTextField!
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var alertLabel: UILabel!
    @IBOutlet weak var tappedAction_backButton: UIButton!
    @IBOutlet weak var otpView: UIView!
    
    override func viewDidLoad(){
        super.viewDidLoad()
        otpView.isHidden = true
        var msgString = ""
        
        if UserDefaults.standard.bool(forKey: "fromAlert") {
            msgString = UserDefaults.standard.value(forKey: "alertNumber") as! String
            alertLabel.text = String(format:"SMS alert is active on %@", msgString)
            
            headerLabel.text = "Alert Preferences"
        }
        else{
            msgString = UserDefaults.standard.value(forKey: "registeredNumber") as! String
            alertLabel.text = String(format:"Current regestered number is %@", msgString)
            headerLabel.text = "Update Number"

        }
        self.navigationController?.isNavigationBarHidden = true
        
    }
    
    @IBAction func tappedAction_backButton(_ sender: Any) {
        let _ = self.navigationController?.popViewController(animated: true)

    }
    
    @IBAction func tappedAction_ChangeNumber(_ sender: Any) {
        self.apiVeryifyOTPLogin()
    }
    
    @IBAction func tappedAction_confirmButton(_ sender: Any) {
        self.apiUpdateNumber()
    }
   
    
    func apiVeryifyOTPLogin() {
        guard let phone = phone_textfield.text, phone != "" else {
            AFWrapperClass.alert(Constants.ApplicationName, message: "Please enter 10 Digit number", view: self)
            return
        }
        
        if UserDefaults.standard.bool(forKey: "fromAlert") {
            let params = ["phone" : "+91" + phone,
                "otp_type" : "alert_preference"] as [String : Any]
            
            NetworkHelper.sharedInstance.sendAyncronousRequest(method: "user/req-update-otp", requestType: "POST", params: params as NSDictionary, showHud: false, completion: { (result) in
                if let dataDict = result as? NSDictionary, let status = dataDict["status"] as? Int, status == 1{
                    self.otpView.isHidden = false
                }else{
                    let dataDict = result as? NSDictionary
                    let message = dataDict?.value(forKey: "error") as? String
                    AFWrapperClass.alert(Constants.ApplicationName, message:message!, view: self)
                }
            }) { (error) in
                AFWrapperClass.alert(Constants.ApplicationName, message: error.localizedDescription, view: self)
            }
        }
        else{
           let params = ["phone" : "+91" + phone,
                      "otp_type" : "update_profile"] as [String : Any]
            
            NetworkHelper.sharedInstance.sendAyncronousRequest(method: "user/req-update-otp", requestType: "POST", params: params as NSDictionary, showHud: false, completion: { (result) in
                if let dataDict = result as? NSDictionary, let status = dataDict["status"] as? Int, status == 1{
                    self.otpView.isHidden = false
                }else{
                    let dataDict = result as? NSDictionary
                    let message = dataDict?.value(forKey: "error") as? String
                    AFWrapperClass.alert(Constants.ApplicationName, message:message!, view: self)
                }
            }) { (error) in
                AFWrapperClass.alert(Constants.ApplicationName, message: error.localizedDescription, view: self)
            }
        }
    }
    
    func apiUpdateNumber() {
    
        guard let phone = phone_textfield.text, phone != "" else {
            AFWrapperClass.alert(Constants.ApplicationName, message: "Please enter 10 Digit number", view: self)
            return
        }
        
        guard let OTP = pincode_textfield.text, OTP != "" else {
            AFWrapperClass.alert(Constants.ApplicationName, message: "Please enter valid OTP", view: self)
            return
        }
        if UserDefaults.standard.bool(forKey: "fromAlert"){
            let params = [
                "phone" : "+91" + phone,
                "otp": OTP,
                "otp_type" : "alert_preference",
                ] as [String : Any]
            
            NetworkHelper.sharedInstance.sendAyncronousRequest(method: "user/update-phone", requestType: "POST", params: params as NSDictionary, showHud: false, completion: { (result) in
                if let dataDict = result as? NSDictionary, let status = dataDict["status"] as? Int, status == 1{
                    self.otpView.isHidden = true
                    self.alertLabel.text = String(format:"SMS alert is now active on +91%@", phone as CVarArg)
                }else{
                    let dataDict = result as? NSDictionary
                    let message = dataDict?.value(forKey: "error") as? String
                    AFWrapperClass.alert(Constants.ApplicationName, message:message!, view: self)
                }
            }) { (error) in
                AFWrapperClass.alert(Constants.ApplicationName, message: error.localizedDescription, view: self)
            }
        }
        else{
            let params = [
                "phone" : "+91" + phone,
                "otp": OTP,
                "otp_type" : "update_profile",
                ] as [String : Any]
            
            NetworkHelper.sharedInstance.sendAyncronousRequest(method: "user/update-phone", requestType: "POST", params: params as NSDictionary, showHud: false, completion: { (result) in
                if let dataDict = result as? NSDictionary, let status = dataDict["status"] as? Int, status == 1{
                    self.otpView.isHidden = true
                    self.alertLabel.text = String(format:"SMS alert is now active on +91%@", phone as CVarArg)
                }else{
                    let dataDict = result as? NSDictionary
                    let message = dataDict?.value(forKey: "error") as? String
                    AFWrapperClass.alert(Constants.ApplicationName, message:message!, view: self)
                }
            }) { (error) in
                AFWrapperClass.alert(Constants.ApplicationName, message: error.localizedDescription, view: self)
            }
        }
        

    }
    
    
}
