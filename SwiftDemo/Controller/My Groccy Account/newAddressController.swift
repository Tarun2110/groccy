//
//  newAddressController.swift
//  SwiftDemo
//
//  Created by Rakesh Kumar on 31/01/18.
//  Copyright © 2018 Rakesh Kumar. All rights reserved.
//


import UIKit

class newAddressController: UIViewController,UIPickerViewDelegate,UIPickerViewDataSource,UITextFieldDelegate{
    
    @IBOutlet weak var addressLine1: UITextField!
    @IBOutlet weak var addressLine2: UITextField!
    @IBOutlet weak var addressNameTextfield: UITextField!
    @IBOutlet weak var cityTextfield: UITextField!
    @IBOutlet weak var areaTextfield: UITextField!
    @IBOutlet weak var pincodeTextfield: UITextField!
    
    let pickerView = UIPickerView()
    var cityArray = [AreaModel]()
    var sectorsArray = [SectorModel]()

    var cityPlace = 0
    var selectedCity = ""
    var selectedArea = ""
    
    override func viewDidLoad(){
        super.viewDidLoad()
        pickerView.delegate = self
        
        cityTextfield.inputView = pickerView
        areaTextfield.inputView = pickerView

        cityTextfield.delegate = self
        cityTextfield.tag = 100
        
        areaTextfield.delegate = self
        areaTextfield.tag = 101
       
        self.navigationController?.isNavigationBarHidden = true
        let paddingView : UIView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 30))
        let paddingViewImage : UIView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 30))
        
        let paddingViewOne : UIView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 30))
        let paddingViewImage2 : UIView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 30))
        
        let imageView = UIImageView(frame: CGRect(x: -10, y: 12.5, width: 10, height: 5))
        let image = UIImage(named: "arrow_down")
        imageView.image = image
        imageView.contentMode = .center
        
        let imageViewTwo = UIImageView(frame: CGRect(x: -10, y: 12.5, width: 10, height: 5))
        let imageTwo = UIImage(named: "arrow_down")
        imageViewTwo.image = imageTwo
        imageViewTwo.contentMode = .center
        
        paddingViewImage.addSubview(imageView)
        paddingViewImage2.addSubview(imageViewTwo)

        cityTextfield.rightView = paddingViewImage
        cityTextfield.leftView = paddingView
        cityTextfield.leftViewMode = .always
        cityTextfield.rightViewMode = .always
        
        areaTextfield.rightView = paddingViewImage2
        areaTextfield.leftView = paddingViewOne
        areaTextfield.leftViewMode = .always
        areaTextfield.rightViewMode = .always
        self.apiGetAllCities()
    }
    
    @IBAction func tappedAction_saveButton(_ sender: Any) {
        if (addressNameTextfield.text?.isEmpty)! {
            AFWrapperClass.alert(Constants.ApplicationName, message: "Please name this address.", view: self)
        }
        else if (addressLine1.text?.isEmpty)! {
            AFWrapperClass.alert(Constants.ApplicationName, message: "Please enter your address.", view: self)
        }
//        else if (cityTextfield.text?.isEmpty)! {
//            AFWrapperClass.alert(Constants.ApplicationName, message: "Please select your city.", view: self)
//        }
//        else if (areaTextfield.text?.isEmpty)! {
//            AFWrapperClass.alert(Constants.ApplicationName, message: "Please select your area.", view: self)
//        }
        else if (pincodeTextfield.text?.isEmpty)! {
            AFWrapperClass.alert(Constants.ApplicationName, message: "Please enter pincode.", view: self)
        }
        else{
            self.apiAddNewAddress()
        }
    }

    func apiAddNewAddress() {
        let params = [
            "name": addressNameTextfield.text!,
            "line1" : addressLine1.text!,
            "line2": addressLine2.text!,
            "pincode": pincodeTextfield.text!,
            "city" : selectedCity,
            "area": selectedArea
        ]
        NetworkHelper.sharedInstance.sendAyncronousRequest(method: "user/addresses", requestType: "POST", params: params as NSDictionary, showHud: true, completion: { (result) in

            if let dataDict = result as? NSDictionary, let status = dataDict["status"] as? Int, status == 1{
                let _ = self.navigationController?.popViewController(animated: true)
            }
            else{
             
            }
        }) { (error) in
            AFWrapperClass.alert(Constants.ApplicationName, message: error.localizedDescription, view: self)
        }
    }
    
    @IBAction func tappedAction_backButton(_ sender: Any) {
        if let navController = self.navigationController {
            navController.popViewController(animated: true)
        }}
    
    
    // MARK:  Picker Delegate
    func numberOfComponents(in pickerView: UIPickerView) -> Int{
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        if cityPlace == 1{
            return self.cityArray.count
        }else if cityPlace == 2{
            return self.sectorsArray.count
        }
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?{
        if cityPlace == 1{
            selectedCity = cityArray[row]._id
            return cityArray[row].name
        }
        else if cityPlace == 2{
            selectedArea = sectorsArray[row].SectorId
            return self.sectorsArray[row].SectorName
        }
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int){
        if cityPlace == 1{
            cityTextfield.text = cityArray[row].name
            cityTextfield.textColor = UIColor.black
        }
        else if cityPlace == 2{
            areaTextfield.text = sectorsArray[row].SectorName
            areaTextfield.textColor = UIColor.black
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.tag == 100 {
            print("chaleya")
            cityPlace = 1
        }
        else if textField.tag == 101{
            cityPlace = 2
             self.apiGetAllArea()
        }
    }
    
    func apiGetAllCities() {
        NetworkHelper.sharedInstance.sendAyncronousRequest(method: "city", requestType: "GET", params: nil, showHud: false, completion: { (result) in
            print(result)
            if let dataDict = result as? NSDictionary, let status = dataDict["status"] as? Int, status == 1, let cityArray = dataDict.value(forKey: "cities") as? NSArray{
                print(cityArray)
                for data in cityArray {
                    if let dic  = data as? NSDictionary {
                        var _id = ""
                        if let areaId = dic["_id"] as? String {
                            _id = areaId
                        }
                        var name = ""
                        if let areaName = dic["name"] as? String {
                            name = areaName
                        }
                        self.cityArray.append(AreaModel.init(_id: _id, name: name))
                        self.pickerView.reloadAllComponents();
                    }}}
            else{
            }
        }) { (error) in
            AFWrapperClass.alert(Constants.ApplicationName, message: error.localizedDescription, view: self)
        }
    }
    
    func apiGetAllArea() {
        let idString = "city/" + selectedCity + "/areas"
        NetworkHelper.sharedInstance.sendAyncronousRequest(method: idString , requestType: "GET", params: nil, showHud: false, completion: { (result) in
            print(result)
            if let dataDict = result as? NSDictionary, let status = dataDict["status"] as? Int, status == 1, let cityArray = dataDict.value(forKey: "areas") as? NSArray{
                self.sectorsArray = SectorModel().getSectorModel(arr: cityArray)
                self.pickerView.reloadAllComponents();
            }
            else{
            }
        }) { (error) in
            AFWrapperClass.alert(Constants.ApplicationName, message: error.localizedDescription, view: self)
        }
    }
}
