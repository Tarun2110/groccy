//
//  checkoutController.swift
//  SwiftDemo
//
//  Created by Rakesh Kumar on 17/04/18.
//  Copyright © 2018 Rakesh Kumar. All rights reserved.
//

import UIKit


class checkoutCellOne: UITableViewCell {
    @IBOutlet weak var addressLineOneLabel: UILabel!
    @IBOutlet weak var addressLineTwoLabel: UILabel!
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var selectPrimaryButton: UIButton!
}


class checkoutCellTwo: UITableViewCell {
    @IBOutlet weak var selectWalletButton: UIButton!
    @IBOutlet weak var totalWalletAmountLabel: UILabel!
    @IBOutlet weak var appicableAmount: UILabel!
}

class checkoutCellFour: UITableViewCell {
    @IBOutlet weak var radioButton: UIButton!
    @IBOutlet weak var optionLabel: UILabel!
}

class checkoutCellThree: UITableViewCell {
    @IBOutlet weak var bagTotalLabel: UILabel!
    @IBOutlet weak var couponDiscountLabel: UILabel!
    @IBOutlet weak var walletDiscountLabel: UILabel!
    @IBOutlet weak var deliveryChargesLabel: UILabel!
    @IBOutlet weak var totalPayableLabel: UILabel!
}





class checkoutController: UIViewController,UITableViewDelegate,UITableViewDataSource{
    var arrayOfAddresses = [AddressModel]()
    @IBOutlet weak var checkoutTableView: UITableView!
    
    @IBOutlet weak var bagTotalLabel: UILabel!
    @IBOutlet weak var couponDiscountLabel: UILabel!
    @IBOutlet weak var walletDiscoutLabel: UILabel!
    @IBOutlet weak var deliveryChargesLabel: UILabel!
    @IBOutlet weak var totalChargesLabel: UILabel!
    @IBOutlet weak var addressLineOne: UILabel!
    @IBOutlet weak var addressLineTwo: UILabel!
    @IBOutlet weak var couponButton: UIButton!
    
    @IBOutlet weak var walletTotalLabel: UILabel!
    @IBOutlet weak var redeemAmountLabel: UILabel!

    @IBOutlet weak var placeOrderButton: UIButton!
    @IBOutlet weak var addressView: UIView!
    @IBOutlet weak var addAddressButton: UIButton!
    var couponApplied = false
    
    override func viewDidLoad(){
        super.viewDidLoad()
        checkoutTableView.delegate = self
        checkoutTableView.dataSource = self
        checkoutTableView.reloadData()
//        if UserDefaults.standard.object(forKey: "addressAvailable") as? Bool == true {
//            addAddressButton.isHidden = true
//            addressView.isHidden = false
//            placeOrderButton.isUserInteractionEnabled = true
//            placeOrderButton.alpha = 1.0
//        }else{
//            addAddressButton.isHidden = false
//            addressView.isHidden = true
//            placeOrderButton.isUserInteractionEnabled = false
//            placeOrderButton.alpha = 0.8
//        }
        
        
//        let totalAmount = UserDefaults.standard.value(forKey: "totalWalletAmount") as? Int
//        let avialableAmount = UserDefaults.standard.value(forKey: "availableRedeemAmount") as? Int

//        walletTotalLabel.text = "Rs. " + (totalAmount?.description)!
//        redeemAmountLabel.text = "Rs. " + (avialableAmount?.description)!

//        addressLineOne.text = UserDefaults.standard.value(forKey: "lineOne") as? String
//        addressLineTwo.text = UserDefaults.standard.value(forKey: "lineTwo") as? String

//        print(UserDefaults.standard.value(forKey: "lineOne") as? String)
//        print(UserDefaults.standard.value(forKey: "lineTwo") as? String)

//        bagTotalLabel.text = UserDefaults.standard.value(forKey: "BagTotal") as? String
//        couponDiscountLabel.text = String(format:"Rs: %@",(UserDefaults.standard.value(forKey: "couponDiscount") as? String)!)
//        totalChargesLabel.text = String(format:"Rs: %@",(UserDefaults.standard.value(forKey: "payableAmount") as? String)!)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.apiGetAddresses()
    }
    
    @IBAction func tappedAction_couponButton(_ sender: Any) {
        let amountToPay = (UserDefaults.standard.value(forKey: "availableRedeemAmount") as! Int)
        let payableAmount = Int((UserDefaults.standard.value(forKey: "payableAmount") as? String)!)
        if amountToPay > 0{
            if let amount = payableAmount, amount <= 2000 {
                if couponApplied == false {
                    totalChargesLabel.text = String(format:"Rs: 0")
                    walletDiscoutLabel.text = String(format:"Rs: %@",(UserDefaults.standard.value(forKey: "payableAmount") as? String)!)
                    couponApplied = true
                    couponButton.setImage(UIImage(named: "checked_active.png"), for: .normal)
                }else{
                    walletDiscoutLabel.text = String(format:"Rs: 0")
                    totalChargesLabel.text = String(format:"Rs: %@",(UserDefaults.standard.value(forKey: "payableAmount") as? String)!)
                    couponApplied = false
                    couponButton.setImage(UIImage(named: "checked_normal.png"), for: .normal)
                }
            }else{
                if couponApplied == false{
                    let pendingAmount = (payableAmount! - amountToPay)
                    couponButton.setImage(UIImage(named: "checked_active.png"), for: .normal)
                    walletDiscoutLabel.text = String(format:"Rs: %@",amountToPay.description)
                    totalChargesLabel.text = String(format:"Rs: %@",pendingAmount.description)
                    couponApplied = true
                }
                else{
                    couponApplied = false
                    couponButton.setImage(UIImage(named: "checked_normal.png"), for: .normal)
                    walletDiscoutLabel.text = String(format:"Rs: 0")
                    totalChargesLabel.text = String(format:"Rs: %@",(payableAmount?.description)!)
                }}
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let vi = UIView.init(frame: CGRect.init(x: 10, y: 0, width: tableView.frame.size.width-20, height: 30))
        let titleLabel = UILabel.init(frame: CGRect.init(x: 10, y: 5, width: tableView.frame.size.width-20, height: 20))
       
        let addButton = UIButton.init(frame: CGRect.init(x: tableView.frame.size.width - 30 , y: 5, width: 20, height: 20))
        addButton.setImage(UIImage(named: "add.png"), for: .normal)
        addButton.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        
        titleLabel.textColor = UIColor.lightGray
        titleLabel.font = UIFont(name:"Montserrat-Regular", size: 13.0)
        
        if section == 0 {
            titleLabel.text = "Addresses"
            vi.addSubview(addButton)
        }
        else if section == 1 {
            titleLabel.text = "Options"
        }
        else if section == 2 {
            titleLabel.text = "Payment Mode"
        }
        else if section == 3 {
            titleLabel.text = "Price Details"
        }
        vi.addSubview(titleLabel)
        return vi
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return self.arrayOfAddresses.count
        }else if section == 2{
            return 2
        }else{
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:checkoutCellOne = self.checkoutTableView.dequeueReusableCell(withIdentifier: "CellOne") as! checkoutCellOne!

        if indexPath.section == 0 {
            let cell:checkoutCellOne = self.checkoutTableView.dequeueReusableCell(withIdentifier: "CellOne") as! checkoutCellOne!
            cell.addressLineOneLabel.text = arrayOfAddresses[indexPath.row].AddressName
            cell.addressLineTwoLabel.text = arrayOfAddresses[indexPath.row].lineOne
            
            cell.editButton.tag = indexPath.row
            cell.selectPrimaryButton.tag = indexPath.row

            let primaryAddress = arrayOfAddresses[indexPath.row].primary.description
            if primaryAddress == "1" {
                cell.selectPrimaryButton.setImage(UIImage(named: "checked_active.png"), for: .normal)
            }else{
                 cell.selectPrimaryButton.setImage(UIImage(named: "checked_normal.png"), for: .normal)
            }
            cell.editButton.addTarget(self, action: #selector(self.deletebuttonClicked(_:)), for:.touchUpInside)
            cell.selectPrimaryButton.addTarget(self, action: #selector(self.selectPrimarybutton(_:)), for:.touchUpInside)
            return cell
        }
        else if indexPath.section == 1 {
            let cell:checkoutCellTwo = self.checkoutTableView.dequeueReusableCell(withIdentifier: "CellTwo") as! checkoutCellTwo!
            if let totalAmount = UserDefaults.standard.value(forKey: "totalWalletAmount") as? Int {
                cell.totalWalletAmountLabel.text = "Rs." + totalAmount.description
            }
            if let appicableAmount = UserDefaults.standard.value(forKey: "availableRedeemAmount") as? Int {
                cell.appicableAmount.text  = "Rs." + appicableAmount.description
            }
            return cell
        }
     
        else if indexPath.section == 2 {
            let cell:checkoutCellFour = self.checkoutTableView.dequeueReusableCell(withIdentifier: "CellFour") as! checkoutCellFour!

            if indexPath.row == 0 {
                cell.optionLabel.text = "COD"
            }else{
                cell.optionLabel.text = "Net Banking"
            }

            cell.radioButton.addTarget(self, action: #selector(self.selectPrimarybutton(_:)), for:.touchUpInside)
        
            
            
            return cell
        }
            
        else if indexPath.section == 3 {
            let cell:checkoutCellThree = self.checkoutTableView.dequeueReusableCell(withIdentifier: "CellThree") as! checkoutCellThree!
            cell.totalPayableLabel.text =  String(format:"Rs: %@",(UserDefaults.standard.value(forKey: "payableAmount") as? String)!)
            return cell
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        if indexPath.section == 0 {
            return 70
        }else if indexPath.section == 1 {
            return 50
        }else if indexPath.section == 2  {
            return 50
        }else if indexPath.section == 3  {
            return 120
        }
        return 0
    }

    @objc func selectPrimarybutton(_ sender:UIButton){
        let addressId = self.arrayOfAddresses[sender.tag]._id.description
        self.apiMakeAddressPrimary(addressId: addressId)
    }

    @objc func deletebuttonClicked(_ sender:UIButton){
    }
    
    @objc func buttonAction(sender: UIButton!) {
       let vc = self.storyboard?.instantiateViewController(withIdentifier: "newAddressController") as! newAddressController
       self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func tappedAction_placeOrder(_ sender: Any) {
        self.apiPlaceOrder()
    }
    
    @IBAction func tappedAction_backButton(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    func apiPlaceOrder() {
        let cartArr = NSMutableArray()
        if couponApplied == true {
            let params = ["use_wallet_balance" : "1"] as [String : Any]
            cartArr.add(params)
        }else{
            let params = ["use_wallet_balance" : "0"] as [String : Any]
            cartArr.add(params)
        }
        NetworkHelper.sharedInstance.sendAyncronousRequest(method: "user/order", requestType: "POST", params:cartArr as Any , showHud: true, completion: { (result) in
        if let dataDict = result as? NSDictionary, let status = dataDict["status"] as? Int, status == 1{
          
            
            let successUrl = dataDict["success_url"] as? String
            let failureUrl = dataDict["failure_url"] as? String

            
            let successUrlString = "http://18.218.188.114/api/" + successUrl!
            let faliureUrlString = "http://18.218.188.114/api/" + failureUrl!

            
            UserDefaults.standard.set(successUrlString, forKey: "successUrl")
            UserDefaults.standard.set(faliureUrlString, forKey: "failureUrl")

           let orderdict = dataDict["order"] as? NSDictionary
           let orderId = orderdict?.value(forKey: "_id")
           let totalAmount = orderdict?.value(forKey: "total") as! Int
           if self.couponApplied == true {
                AFWrapperClass.alert(Constants.ApplicationName, message: "Your order has been successfully placed.", view: self)
                DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                    _ = self.navigationController?.popViewController(animated: true)
                }}
            else{
                self.goToPayUMoney(orderID: orderId  as! NSString, amount: totalAmount as Int)
            }}
        else{
            }
        }) { (error) in
            AFWrapperClass.alert(Constants.ApplicationName, message: error.localizedDescription, view: self)
        }
    }
    
    func apiGetAddresses() {
        NetworkHelper.sharedInstance.sendAyncronousRequest(method: "user/addresses", requestType: "GET", params: nil, showHud: false, completion: { (result) in
            if let dataDict = result as? NSDictionary, let status = dataDict["status"] as? Int, status == 1{
                let addressesArray = dataDict.value(forKey: "addresses") as? NSArray
                if addressesArray?.count != 0{
                    self.arrayOfAddresses = AddressModel().getAddressModel(arr: addressesArray!)
                    print(self.arrayOfAddresses)
                    self.checkoutTableView.delegate = self
                    self.checkoutTableView.dataSource = self
                    self.checkoutTableView.reloadData()
                }
                else{
                }
            }
            else{
            }
        }) { (error) in
            AFWrapperClass.alert(Constants.ApplicationName, message: error.localizedDescription, view: self)
        }
    }
    
    
    func apiMakeAddressPrimary (addressId:String) {
        let methodString = "user/address/primary/" + addressId
        NetworkHelper.sharedInstance.sendAyncronousRequest(method: methodString, requestType: "GET", params: nil, showHud: false, completion: { (result) in
            if let dataDict = result as? NSDictionary, let status = dataDict["status"] as? Int, status == 1{
                self.apiGetAddresses()
            }
            else{
            }
        }) { (error) in
            AFWrapperClass.alert(Constants.ApplicationName, message: error.localizedDescription, view: self)
        }
    }
    
    func goToPayUMoney(orderID: NSString ,amount: NSInteger){
        let pumvc = self.storyboard?.instantiateViewController(withIdentifier: "PaymentPageViewController") as! PaymentPageViewController
        pumvc.orderId = orderID as String!
        pumvc.name = "test user"
        pumvc.amount = amount.description
        pumvc.phone = "8143333333"
        pumvc.email = "test@gmail.com"
        pumvc.productInfo = "Test"
        self.navigationController?.pushViewController(pumvc, animated: true)
    }
}
