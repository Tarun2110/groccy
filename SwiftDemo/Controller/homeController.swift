//
//  homeController.swift
//  SwiftDemo
//
//  Created by Rakesh Kumar on 08/01/18.
//  Copyright © 2018 Rakesh Kumar. All rights reserved.
//

import UIKit
import LCBannerView
import SDWebImage

// MARK:  Models Methods

struct categoriesModel {
    var catId = ""
    var catImage = ""
    var catName = ""
    var catparent = ""
    var catSubCategories = [subCategoryModel]()

    
    func getCategoryModel(arr: NSArray) -> [categoriesModel] {
        var CatAddresses = [categoriesModel]()
        for data in arr {
            if let dict = data as? NSDictionary {
                var catObject = categoriesModel()
                if let sectorId = dict["_id"] as? String {
                    catObject.catId = sectorId
                }
                if let sectorName = dict["name"] as? String {
                    catObject.catName = sectorName
                }
                if let imageCategory = dict["category_icon"] as? String {
                    catObject.catImage = imageCategory
                }
                if let catSubCategories = dict["subcategories"] as? NSArray {
                    catObject.catSubCategories = subCategoryModel().getsubCategoryModel(arr: catSubCategories)
                }
                
                CatAddresses.append(catObject)
            }}
        return CatAddresses
    }
}


struct BannersModel {
    var BannerId = ""
    var BannerImage = ""
    var BannerName = ""
    func getBannerModel(arr: NSArray) -> [BannersModel] {
        var BannerAddresses = [BannersModel]()
        for data in arr {
            if let dict = data as? NSDictionary {
                var BannerObject = BannersModel()
                if let bannerId = dict["_id"] as? String {
                    BannerObject.BannerId = bannerId
                }
                if let bannerName = dict["name"] as? String {
                    BannerObject.BannerName = bannerName
                }
                if let bannerImage = dict["image"] as? String {
                    BannerObject.BannerImage = bannerImage
                }
                BannerAddresses.append(BannerObject)
            }}
        return BannerAddresses
    }
}


struct PackageModel {
    var PackId = ""
    var PackIconImage = ""
    var PackHeading = ""
    var PackButtonTitle = ""
    var PackShortDes = ""
    var PackBannerimage = ""
    var PackDescription = ""

    func getPackageModel(arr: NSArray) -> [PackageModel] {
        var packagesAddress = [PackageModel]()
        for data in arr {
            if let dict = data as? NSDictionary {
                var packageObject = PackageModel()
                if let BId = dict["_id"] as? String {
                    packageObject.PackId = BId
                }
                if let iconImage = dict["icon_image"] as? String {
                    packageObject.PackIconImage = iconImage
                }
                if let headindTitle = dict["heading"] as? String {
                    packageObject.PackHeading = headindTitle
                }
                
                if let ButtonTitle = dict["name"] as? String {
                    packageObject.PackButtonTitle = ButtonTitle
                }
                if let shortDes = dict["short_description"] as? String {
                    packageObject.PackShortDes = shortDes
                }
                if let bannarImage = dict["banner_image"] as? String {
                    packageObject.PackBannerimage = bannarImage
                }
                if let packDes = dict["description"] as? String {
                    packageObject.PackDescription = packDes
                }
                packagesAddress.append(packageObject)
            }}
        return packagesAddress
    }
}

struct BrandsModel {
    var BrandId = ""
    var BrandLogo = ""
    var BrandName = ""
    var __v = ""
    
    func getBrandsModel(arr: NSArray) -> [BrandsModel] {
        var BrandAddress = [BrandsModel]()
        for data in arr {
            if let dict = data as? NSDictionary {
                var packageObject = BrandsModel()
                if let BId = dict["_id"] as? String {
                    packageObject.BrandId = BId
                }
                if let iconImage = dict["brand_logo"] as? String {
                    packageObject.BrandLogo = iconImage
                }
                if let headindTitle = dict["name"] as? String {
                    packageObject.BrandName = headindTitle
                }
                BrandAddress.append(packageObject)
            }}
        return BrandAddress
    }
}

struct sponseredModel {
    var sponseredId = ""
    var sponseredName = ""
    var sponseredImage = ""

    func getsponseredModel(arr: NSArray) -> [sponseredModel] {
        var sponseredAddress = [sponseredModel]()
        for data in arr {
            if let dict = data as? NSDictionary {
                var sponseredObject = sponseredModel()
                if let s_id = dict["_id"] as? String {
                    sponseredObject.sponseredId = s_id
                }
                if let SImage = dict["image"] as? String {
                    sponseredObject.sponseredImage = SImage
                }
                if let productName = dict["name"] as? String {
                    sponseredObject.sponseredName = productName
                }
                sponseredAddress.append(sponseredObject)
            }}
        return sponseredAddress
    }
}


struct subCategoryModel {
    var sCategoryId = ""
    var sCategoryName = ""
    var sCategoryImage = ""
    
    func getsubCategoryModel(arr: NSArray) -> [subCategoryModel] {
        var subCategoryAddress = [subCategoryModel]()
        for data in arr {
            if let dict = data as? NSDictionary {
                var subCategoryObject = subCategoryModel()
                if let s_id = dict["_id"] as? String {
                    subCategoryObject.sCategoryId = s_id
                }
                if let SImage = dict["image"] as? String {
                    subCategoryObject.sCategoryImage = SImage
                } else if let category_icon = dict["category_icon"] as? String {
                    subCategoryObject.sCategoryImage = category_icon
                }
                if let productName = dict["name"] as? String {
                    subCategoryObject.sCategoryName = productName
                }
                
                subCategoryAddress.append(subCategoryObject)
            }}
        return subCategoryAddress
    }
}



// MARK:  Table Custom Cell

class HomeTableViewCell: UITableViewCell{
    
    @IBOutlet weak var packageImage: UIImageView!
    @IBOutlet weak var packageButton: UIButton!
    @IBOutlet weak var packageTitle: UILabel!
}

class ExclusiveCategoryCVCell: UICollectionViewCell {
    @IBOutlet weak var categoryBackgroundView: UIView!
    @IBOutlet weak var categoryName: UILabel!
    @IBOutlet weak var categoryImage: UIImageView!
}

class BrandCustomCell: UICollectionViewCell {
    @IBOutlet weak var productImageView: UIImageView!
}

class subCategoryCell: UICollectionViewCell {
    @IBOutlet weak var subCategoryName: UILabel!

}

class sponseredCustomCell: UICollectionViewCell {
    @IBOutlet weak var sponseredImage: UIImageView!
}




class homeController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, LCBannerViewDelegate, UICollectionViewDelegateFlowLayout,UITableViewDataSource,UITableViewDelegate{
    @IBOutlet weak var categoryCollectionView: UICollectionView!
    @IBOutlet weak var bannerGroundView: UIView!
    @IBOutlet weak var brandsCollectionView: UICollectionView!
    @IBOutlet weak var subCategoryCollectionView: UICollectionView!

    @IBOutlet weak var sponseredCollectionView: UICollectionView!
    
    
    var  arrayOfBanners = [BannersModel]()
    var appDel = AppDelegate()
    var  arrayOfBrands = [BrandsModel]()
    var  arrayOfFooterBanners = [BannersModel]()
    var  arrayOfSponseredProducts = [sponseredModel]()
    
    var  arrayOfSubCategories = [subCategoryModel]()

    @IBOutlet weak var cartCountLabel: UILabel!
    @IBOutlet weak var categoriesCVHeightConstrain: NSLayoutConstraint!
    @IBOutlet weak var packagesTBeightConstrain: NSLayoutConstraint!
    @IBOutlet weak var sponseredCVHeightConstrain: NSLayoutConstraint!

    @IBOutlet weak var packagesTableView: UITableView!
    @IBOutlet weak var footerImage: UIImageView!
    @IBOutlet weak var footerBannerView: UIView!
  
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    // MARK:  View Setup
    override func viewDidLoad() {
        super.viewDidLoad()
        appDel = UIApplication.shared.delegate as! AppDelegate
        self.categoryCollectionView.showsHorizontalScrollIndicator = false
        self.categoryCollectionView.showsVerticalScrollIndicator = false
        self.navigationController?.isNavigationBarHidden = true
        self.apiHomeDetails()
        
    }

    // MARK:   Tap Actions on Button
    @IBAction func tappedAction_cartButton(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "cartController") as! cartController
        self.navigationController?.pushViewController(vc, animated: true)
    }
   
    @IBAction func tappedAction_toggle(_ sender: Any) {
        sideMenuVC.toggleMenu()
    }

    
    //Links
    
    @IBAction func TappedAction_linkedUpControllers(_ sender: UIButton) {
        if sender.tag == 10 || sender.tag == 20 {
            UserDefaults.standard.set(true, forKey: "fromHome")
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "packageController") as! packageController
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        if sender.tag == 40 {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "easyReturnController") as! easyReturnController
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    
    // MARK:   CollectionView Methods

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == categoryCollectionView{
            return self.appDel.arrayOfCategoreies.count
        }else if collectionView == brandsCollectionView {
            return self.arrayOfBrands.count
        }else if collectionView == sponseredCollectionView {
            return self.arrayOfSponseredProducts.count
        }else{
            return self.arrayOfSubCategories.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
         if collectionView == categoryCollectionView{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ExclusiveCategoryCVCell", for: indexPath as IndexPath) as! ExclusiveCategoryCVCell
            cell.categoryName.text = self.appDel.arrayOfCategoreies[indexPath.row].catName.uppercased()
            cell.categoryImage.sd_setImage(with: URL(string: self.appDel.arrayOfCategoreies[indexPath.row].catImage), placeholderImage: UIImage(named: "placeholder.png"))
            cell.categoryImage.contentMode = .scaleAspectFit
            print(self.appDel.arrayOfCategoreies[indexPath.row].catImage)
            return cell
        }
        else if collectionView == brandsCollectionView{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BrandCustomCell", for: indexPath as IndexPath) as! BrandCustomCell
            cell.productImageView.sd_setImage(with: URL(string: self.arrayOfBrands[indexPath.row].BrandLogo), placeholderImage: UIImage(named: "placeholder.png"))
            cell.productImageView.contentMode = .scaleAspectFit
            return cell
         }else if collectionView == sponseredCollectionView{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "sponseredCell", for: indexPath as IndexPath) as! sponseredCustomCell
            cell.sponseredImage.sd_setImage(with: URL(string: self.arrayOfSponseredProducts[indexPath.row].sponseredImage), placeholderImage: UIImage(named: "placeholder.png"))
            cell.sponseredImage.contentMode = .scaleAspectFill
            return cell
         }
         
         else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "subCategoryCell", for: indexPath as IndexPath) as! subCategoryCell
            cell.subCategoryName.text = self.arrayOfSubCategories[indexPath.row].sCategoryName
            return cell
        }
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == categoryCollectionView{
            products.removeAll()
            let selectId =  self.appDel.arrayOfCategoreies[indexPath.row].catId
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "buyItemsController") as! buyItemsController
            vc.CategoryID = selectId
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == subCategoryCollectionView{
            let str = self.arrayOfSubCategories[indexPath.row].sCategoryName
            let width = str.width(withConstraintedHeight: 40, font: UIFont.systemFont(ofSize: 30))
            return CGSize(width: width - 10, height: 40);
        } else if collectionView == categoryCollectionView {
            return CGSize(width: collectionView.frame.size.width/3, height: collectionView.frame.size.width/3);
        } else if collectionView == brandsCollectionView {
            return CGSize(width: collectionView.frame.size.width/3, height: collectionView.frame.size.height)
        } else {
            return CGSize(width: collectionView.frame.size.width/2, height: collectionView.frame.size.width/2)
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
       
        if collectionView == subCategoryCollectionView{
            return 2.0
        }
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        if collectionView == subCategoryCollectionView{
            return 2.0
        }
        return 0.0
    }
    
    // MARK:  TableView Delegate Methods

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return appDel.arrayOfPackages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:HomeTableViewCell = self.packagesTableView.dequeueReusableCell(withIdentifier: "packageCell") as! HomeTableViewCell!
        cell.packageTitle.text = appDel.arrayOfPackages[indexPath.row].PackButtonTitle.uppercased()
        cell.packageButton.setTitle(appDel.arrayOfPackages[indexPath.row].PackHeading,for: .normal)
        
        
        
        if cell.packageTitle.text == "GROCCY GOLD PACKAGE" {
            cell.packageImage.image = #imageLiteral(resourceName: "bg_gold")
        }else if cell.packageTitle.text == "GROCCY SILVER PACKAGE"{
            cell.packageImage.image = #imageLiteral(resourceName: "bg_silver")
        }else{
            cell.packageImage.image = #imageLiteral(resourceName: "bg_trial")
        }
       return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "packDetailController") as! packDetailController
        vc.packageModel = appDel.arrayOfPackages[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    
    func apiHomeDetails() {
        NetworkHelper.sharedInstance.sendAyncronousRequest(method: "home", requestType: "GET", params: nil, showHud: true, completion: { (result) in
            if let dataDict = result as? NSDictionary, let status = dataDict["status"] as? Int, status == 1{
                let categoriesArray = dataDict.value(forKey: "categories") as? NSArray
                self.appDel.arrayOfCategoreies = categoriesModel().getCategoryModel(arr: categoriesArray!)
                self.categoryCollectionView.delegate = self
                self.categoryCollectionView.dataSource = self
                self.categoriesCVHeightConstrain.constant = 200
                self.categoryCollectionView.reloadData()
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    let value = self.categoryCollectionView.contentSize.height
                    self.categoriesCVHeightConstrain.constant = value
                }
                if let aboutString = dataDict.value(forKey: "about") as? NSString {
                    UserDefaults.standard.set(aboutString, forKey: "aboutUs")
                }
                
                if let wallet_amountString = dataDict.value(forKey: "wallet_amount") as? Int {
                    UserDefaults.standard.set(wallet_amountString, forKey: "walletAmount")
                    UserDefaults.standard.synchronize()
                }
                
                if let packagesArray = dataDict.value(forKey: "packages") as? NSArray {
                    self.appDel.arrayOfPackages = PackageModel().getPackageModel(arr: packagesArray)
                    self.packagesTableView.delegate = self
                    self.packagesTableView.dataSource = self

                    self.packagesTBeightConstrain.constant = 200
                    self.packagesTableView.reloadData()
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                        let value = self.packagesTableView.contentSize.height
                        self.packagesTBeightConstrain.constant = value
                    }
                }
                
                if let sponseredArray = dataDict.value(forKey: "sponsored_products") as? NSArray {
                    self.arrayOfSponseredProducts = sponseredModel().getsponseredModel(arr: sponseredArray)
                    self.sponseredCollectionView.delegate = self
                    self.sponseredCollectionView.dataSource = self
                    self.sponseredCVHeightConstrain.constant = 200
                  
                    let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
                    layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
                    layout.itemSize = CGSize(width: UIScreen.main.bounds.width/2, height: UIScreen.main.bounds.width/2)
                    layout.minimumInteritemSpacing = 0
                    layout.minimumLineSpacing = 0
                    self.sponseredCollectionView!.collectionViewLayout = layout
                    self.sponseredCollectionView.reloadData()
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                        let value = self.sponseredCollectionView.contentSize.height
                        self.sponseredCVHeightConstrain.constant = value
                    }
                }

                if let subCategoriesArray = dataDict.value(forKey: "subcategories") as? NSArray {
                    self.arrayOfSubCategories = subCategoryModel().getsubCategoryModel(arr: subCategoriesArray)
                    self.subCategoryCollectionView.delegate = self
                    self.subCategoryCollectionView.dataSource = self
                    self.subCategoryCollectionView.reloadData()
                }
                
                if let BrandsArray = dataDict.value(forKey: "brands") as? NSArray {
                    self.arrayOfBrands = BrandsModel().getBrandsModel(arr: BrandsArray)
                    self.brandsCollectionView.delegate = self
                    self.brandsCollectionView.dataSource = self
                    self.brandsCollectionView.reloadData()
                }
                
                if let cartCountString = dataDict.value(forKey: "cart_count") as? Int {
                    self.cartCountLabel.text = cartCountString.description
                    UserDefaults.standard.set(cartCountString.description, forKey: "cartCount")
                }
                
                if let bannersArray = dataDict.value(forKey: "banners") as? NSArray {
                    self.arrayOfBanners = BannersModel().getBannerModel(arr: bannersArray)
                }

                
                if let footerblog = dataDict["blog"] as? NSDictionary, let bannerImage = footerblog["banner"] as? String , let bannerlink =  footerblog["link"] as? String{
                    print(bannerlink)
                    self.footerImage.sd_setImage(with: URL(string: bannerImage), placeholderImage: UIImage(named: "placeholder.png"))
                    self.footerImage.contentMode = .scaleToFill
                }
                
                var bannerUrls = [String]()
                for banner in self.arrayOfBanners {
                    bannerUrls.append(banner.BannerImage)
                }
                if self.arrayOfBanners.count != 0 {
                    let bannerrLC = LCBannerView.init(frame: CGRect.init(x: 0, y: 0, width: self.bannerGroundView.frame.size.width, height:self.bannerGroundView.frame.size.height), delegate: self, imageURLs: bannerUrls, placeholderImageName: "", timeInterval: 5, currentPageIndicatorTintColor: #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1), pageIndicatorTintColor: #colorLiteral(red: 0.8349592707, green: 0.1440922479, blue: 0.04805831931, alpha: 1))
                    
                     let FooterbannerrLC = LCBannerView.init(frame: CGRect.init(x: 0, y: 0, width: self.bannerGroundView.frame.size.width, height:self.bannerGroundView.frame.size.height), delegate: self, imageURLs: bannerUrls, placeholderImageName: "", timeInterval: 5, currentPageIndicatorTintColor: #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1), pageIndicatorTintColor: #colorLiteral(red: 0.8349592707, green: 0.1440922479, blue: 0.04805831931, alpha: 1))
                    
                    self.bannerGroundView.addSubview(bannerrLC!)
                    self.footerBannerView.addSubview(FooterbannerrLC!)
                }
            }
            else{
            }
        })
        { (error) in
            AFWrapperClass.alert(Constants.ApplicationName, message: error.localizedDescription, view: self)
        }
    }

   

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
