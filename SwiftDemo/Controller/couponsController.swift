//
//  couponsController.swift
//  SwiftDemoTests
//
//  Created by Rakesh Kumar on 11/04/18.
//  Copyright © 2018 Rakesh Kumar. All rights reserved.
//

import UIKit


class couponCell: UITableViewCell {
    @IBOutlet weak var couponName: UILabel!
    @IBOutlet weak var validTillLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var applyButton: UIButton!
}


class couponsController: UIViewController,UITableViewDelegate,UITableViewDataSource{
    @IBOutlet weak var couponTableView: UITableView!
    var couponsArray =  [Any]()
    let cellReuseIdentifier = "customCouponCell"

    override func viewDidLoad(){
        super.viewDidLoad()
        self.apiGetCouponsList()
    }
    
    @IBAction func tappedAction_Backbutton(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return couponsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:couponCell = self.couponTableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! couponCell!
        cell.selectionStyle = UITableViewCellSelectionStyle.none

        if let dic = self.couponsArray[indexPath.row] as? NSDictionary {
            let couponTextName = dic.object(forKey: "coupon_type") as? String
            let amount = dic.object(forKey: "amount") as? Int
            let validTill = dic.object(forKey: "expired_at") as? String

            let nameString = NSString(format: "%@%@", couponTextName!, (amount?.description)!)
            let amountString = NSString(format: "Rs. %@ Off",(amount?.description)!)
       
            let max = validTill ?? ""
            cell.validTillLabel.text = "Valid till" + max
            
            cell.couponName.text = nameString.uppercased
            cell.priceLabel.text = amountString as String
            
            let coupounStatus = dic.object(forKey: "status") as? String
            if coupounStatus == "available" {
               cell.applyButton.setImage(UIImage(named: "checked_normal.png"), for: .normal)
            }
            else{
               cell.applyButton.setImage(UIImage(named: "checked_active.png"), for: .normal)
            }
            
            cell.applyButton.tag = indexPath.row
            cell.applyButton.addTarget(self, action: #selector(self.buttonClicked(_:)), for:.touchUpInside)
        }
        return cell
    }
   
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
            return 90;
    }
    
    @objc func buttonClicked(_ sender:UIButton){
        if let dic = self.couponsArray[sender.tag] as? NSDictionary {
            let couponid = dic.object(forKey: "_id") as? String
            self.apiApplyCoupon(couponId: couponid!)
        }
    }
    
    func apiApplyCoupon (couponId:String) {
        let parameters = ["coupon": couponId] as [String : Any]
        NetworkHelper.sharedInstance.sendAyncronousRequest(method: "user/coupon/apply", requestType: "POST", params: parameters as Any, showHud: true, completion: { (result) in
        print(result)
            if let dataDict = result as? NSDictionary, let status = dataDict["status"] as? Int, status == 1{
                AFWrapperClass.alert(Constants.ApplicationName, message: "Coupon has been applied.", view: self)
            }
            else{
                if let dataDict = result as? NSDictionary, let status = dataDict["message"] as? String{
                    AFWrapperClass.alert(Constants.ApplicationName, message: status, view: self)
                }
            }
            self.apiGetCouponsList()
        }) { (error) in
            AFWrapperClass.alert(Constants.ApplicationName, message: "Something went wrong.", view: self)
        }
    }
    
    func apiGetCouponsList() {
        NetworkHelper.sharedInstance.sendAyncronousRequest(method: "user/coupons", requestType: "GET", params: nil, showHud: true, completion: { (result) in
            if let dataDict = result as? NSDictionary, let status = dataDict["status"] as? Int, status == 1{
                if let couponsValueArray = dataDict.value(forKey: "coupons") as? NSArray{
                    self.couponsArray = couponsValueArray as! [Any]
                    let cartCount = couponsValueArray.count
                    if cartCount > 0 {
                        self.couponTableView.delegate = self;
                        self.couponTableView.dataSource = self;
                        self.couponTableView.reloadData()
                    }}}
            else{
            }
        }) { (error) in
            AFWrapperClass.alert(Constants.ApplicationName, message: error.localizedDescription, view: self)
        }
    }
}



