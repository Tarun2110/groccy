 
 //
//  AppDelegate.swift
//  SwiftDemo
//
//  Created by Rakesh Kumar on 06/01/18.
//  Copyright © 2018 Rakesh Kumar. All rights reserved.
//

import Foundation
import UIKit
import IQKeyboardManagerSwift
let kConstantObj = kConstant()
import Google
import FBSDKCoreKit
import FBSDKLoginKit
import GoogleSignIn
import UserNotifications
import Firebase
import FirebaseInstanceID
import FirebaseMessaging

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, GIDSignInDelegate,UNUserNotificationCenterDelegate {
  
    var  arrayOfPackages = [PackageModel]()
    var  arrayOfCategoreies = [categoriesModel]()
    
    public func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if (error == nil) {
            
        } else {
            print("\(error.localizedDescription)")
        }
    }
    
  
    
    var window: UIWindow?
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        GIDSignIn.sharedInstance().delegate = self
        IQKeyboardManager.sharedManager().enable = true
        
        FIRApp.configure()
        if UserDefaults.standard.bool(forKey: "LoggedIn"){
            let mainVcIntial = kConstantObj.SetIntialMainViewController("homeController")
            self.window?.rootViewController = mainVcIntial
        }
        else{
            let mainVcIntial = kConstantObj.SetIntialMainViewController("landingController")
            self.window?.rootViewController = mainVcIntial
        }
        
//        UNUserNotificationCenter.current().getNotificationSettings(){ (settings) in
//            
//            switch settings.soundSetting{
//            case .enabled:
//                
//                print("enabled sound setting")
//                
//            case .disabled:
//                
//                print("setting has been disabled")
//                
//            case .notSupported:
//                print("something vital went wrong here")
//            }
//        }
        
        
        
        let center = UNUserNotificationCenter.current()
        center.requestAuthorization(options:[.badge, .alert, .sound]) { (granted, error) in
            // Enable or disable features based on authorization.
        }
        application.registerForRemoteNotifications()
        
        self.connectToFcm()
        
        return FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
    }
    
  
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        if let refreshedToken = FIRInstanceID.instanceID().token() {
            print("InstanceID token: \(refreshedToken)")

        }
    }
    
    func connectToFcm() {
        guard FIRInstanceID.instanceID().token() != nil else  {
            return
        }
        
        FIRMessaging.messaging().disconnect()
        FIRMessaging.messaging().connect { (error) in
            
            if (error != nil) {
                debugPrint("Unable to connect with FCM. \(String(describing: error))")
            } else {
                debugPrint("Connected to FCM.")
                if let deviceTokenStr = FIRInstanceID.instanceID().token() {
                    UserDefaults.standard.set(deviceTokenStr, forKey: "deviceToken")
                    print("added to app store ---> \(deviceTokenStr)")
                }
            }
        }
    }
    
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Failed to register:", error)
    }
    
    func registrationhandler(_ registrationToken: String!, error: NSError!) {
        if (registrationToken != nil) {
            debugPrint("registrationToken = \(String(describing: registrationToken))")
        } else {
            debugPrint("Registration to FCM failed with error: \(error.localizedDescription)")
        }
    }
    
    
    func applicationWillResignActive(_ application: UIApplication) {
        FBSDKAppEvents.activateApp()
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        if !url.absoluteString.contains("127559377817993") {
            return GIDSignIn.sharedInstance().handle(url, sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as! String!, annotation: options[UIApplicationOpenURLOptionsKey.annotation])
        } else {
            return FBSDKApplicationDelegate.sharedInstance().application(app, open: url, sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as! String!, annotation: options[UIApplicationOpenURLOptionsKey.annotation])
        }
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user:GIDGoogleUser!,
              withError error: Error!) {
        // Perform any operations when the user disconnects from app here.
        // ...
    }
    
   
 
}

