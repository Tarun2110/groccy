//
//  landingController.swift
//  SwiftDemo
//
//  Created by Rakesh Kumar on 08/01/18.
//  Copyright © 2018 Rakesh Kumar. All rights reserved.
//
import Foundation
import UIKit
import FBSDKLoginKit
import GoogleSignIn

import MessageUI

class AreaModel: NSObject, NSCoding {
    var _id = ""
    var name = ""
    
    init(_id: String, name: String) {
        self._id = _id
        self.name = name
    }
    required convenience init(coder aDecoder: NSCoder) {
        let _id = aDecoder.decodeObject(forKey: "_id") as! String
        let name = aDecoder.decodeObject(forKey: "name") as! String
        self.init(_id: _id, name: name)
    }
    func encode(with aCoder: NSCoder) {
        aCoder.encode(_id, forKey: "_id")
        aCoder.encode(name, forKey: "name")
    }
}

struct SectorModel {
    var SectorId = ""
    var SectorName = ""
    
    func getSectorModel(arr: NSArray) -> [SectorModel] {
        var SectorAddresses = [SectorModel]()
        for data in arr {
            if let dict = data as? NSDictionary {
                var area = SectorModel()
                if let sectorId = dict["_id"] as? String {
                    area.SectorId = sectorId
                }
                if let sectorName = dict["name"] as? String {
                    area.SectorName = sectorName
                }
                SectorAddresses.append(area)
            }}
        return SectorAddresses
    }
}


class landingController: UIViewController, UIPickerViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIPickerViewDataSource,UIGestureRecognizerDelegate,MFMailComposeViewControllerDelegate{
    
    @IBOutlet weak var selectPlaceView: UIView!
    @IBOutlet weak var selectAreaView: UIView!
    @IBOutlet weak var selectedAreaView: UIView!
    @IBOutlet weak var phoneRegisterView: UIView!
    @IBOutlet weak var otpView: UIView!
    
    @IBOutlet weak var pinCodeTextField: PinCodeTextField!
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var areaTextfield: UITextField!
    @IBOutlet weak var placeTextfield: UITextField!
    @IBOutlet weak var phoneTextfield: UITextField!

    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var continueButton: UIButton!
    
    var arrayOfSectors = [SectorModel]()
    
    var pickOption = [AreaModel]()
    var username = ""
    var userId = ""
    var selectedCityId = ""
    var selectedAreaId = ""
    
    var place = 1
    
    var dict : [String : AnyObject]!
    var socialId = ""
    var socialFirstName = ""
    var socialLastName = ""
    var socialMail = ""
    var socialImgUrlStr = ""
    var picker = UIImagePickerController()
    var deviceToken = ""
    
    @IBOutlet weak var selectedPlaceLabel: UILabel!
    @IBOutlet weak var otpLabel: UILabel!
    @IBOutlet weak var backgroundImage: UIImageView!
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad(){
        super.viewDidLoad()
        let pickerView = UIPickerView()
        pickerView.delegate = self
        
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        phoneRegisterView.alpha = 0.0
        selectAreaView.alpha = 0.0
        selectedAreaView.alpha = 0.0
        otpView.alpha = 0.0
        
        backButton.alpha = 0.0
        
        // MARK: DropShadowForTheViews
        selectPlaceView.layer.shadowOpacity = 0.8
        selectPlaceView.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        selectPlaceView.layer.shadowRadius = 15.0
        selectPlaceView.layer.shadowColor = UIColor.gray.cgColor
        
        selectAreaView.layer.shadowOpacity = 0.8
        selectAreaView.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        selectAreaView.layer.shadowRadius = 15.0
        selectAreaView.layer.shadowColor = UIColor.gray.cgColor
        
        selectedAreaView.layer.shadowOpacity = 0.8
        selectedAreaView.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        selectedAreaView.layer.shadowRadius = 15.0
        selectedAreaView.layer.shadowColor = UIColor.gray.cgColor
        
        phoneRegisterView.layer.shadowOpacity = 0.8
        phoneRegisterView.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        phoneRegisterView.layer.shadowRadius = 15.0
        phoneRegisterView.layer.shadowColor = UIColor.gray.cgColor
        
        otpView.layer.shadowOpacity = 0.8
        otpView.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        otpView.layer.shadowRadius = 15.0
        otpView.layer.shadowColor = UIColor.gray.cgColor
    
        // MARK: TextFieldCornerRadius
       // placeTextfield.layer.cornerRadius = 20.0
        placeTextfield.layer.borderWidth = 1.0
        placeTextfield.layer.borderColor = UIColor.red.cgColor
        placeTextfield.inputView = pickerView
        
     //   areaTextfield.layer.cornerRadius = 20.0
        areaTextfield.layer.borderWidth = 1.0
        areaTextfield.layer.borderColor = UIColor.red.cgColor
        areaTextfield.inputView = pickerView
        
        // MARK: PaddingForTextfield
        let paddingView : UIView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 35))
        let paddingViewR : UIView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 35))
        
        let imageView = UIImageView(frame: CGRect(x: -10, y: 12.5, width: 10, height: 5))
        let image = UIImage(named: "arrow_down")
        imageView.image = image
        imageView.contentMode = .center
        
        paddingViewR.addSubview(imageView)
        placeTextfield.rightView = paddingViewR
        placeTextfield.leftView = paddingView
        placeTextfield.leftViewMode = .always
        placeTextfield.rightViewMode = .always
        placeTextfield.placeholder = "Select your city"
        placeTextfield.textColor = UIColor.lightGray
        
        
        let paddingLeft : UIView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 35))
        let paddingRight : UIView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 35))
        
        let imageV = UIImageView(frame: CGRect(x: -10, y: 12.5, width: 10, height: 5))
        imageV.image = image
        imageV.contentMode = .center
        
        paddingViewR.addSubview(imageView)
        areaTextfield.rightView = paddingRight
        areaTextfield.leftView = paddingLeft
        areaTextfield.leftViewMode = .always
        areaTextfield.rightViewMode = .always
        areaTextfield.placeholder = "Select near by area"
        areaTextfield.textColor = UIColor.lightGray
        
        titleLabel.text = "GROCCY MEANS QUALITY"

        
        self.apiGetAllCities()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let screenSize = UIScreen.main.bounds
        if screenSize.height == 812 {
            self.backgroundImage.image = #imageLiteral(resourceName: "login_bg_1x")
        } else {
            self.backgroundImage.image = #imageLiteral(resourceName: "login_bg_1")
        }
    }
    
    
    
    // MARK:  Picker Delegate
    func numberOfComponents(in pickerView: UIPickerView) -> Int{
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        if place == 1{
            return self.pickOption.count
        }
        else{
            return self.arrayOfSectors.count
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?{
        if place == 1{
            selectedCityId = pickOption[row]._id
            return pickOption[row].name
        }
        else{
            selectedAreaId = arrayOfSectors[row].SectorId
            return arrayOfSectors[row].SectorName
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int){
        if place == 1{
            placeTextfield.text = pickOption[row].name
            selectedCityId = pickOption[row]._id
            placeTextfield.textColor = UIColor.black
        }
        else{
            areaTextfield.text = arrayOfSectors[row].SectorName
            selectedAreaId = arrayOfSectors[row].SectorId
            areaTextfield.textColor = UIColor.black
        }
    }
    
    // MARK:  Button Actions
    @IBAction func tappedAction_backbutton(_ sender: Any){
        UIView.animate(withDuration: 0.5, animations:{
            self.selectPlaceView.alpha = 1.0
            self.phoneRegisterView.alpha = 0.0
            self.otpView.alpha = 0.0
            self.selectedAreaView.alpha = 0.0
            self.backButton.alpha = 0.0
            self.selectAreaView.alpha = 0.0
            
            self.placeTextfield.placeholder = "Select your city"
            self.placeTextfield.textColor = UIColor.lightGray
            
            self.areaTextfield.placeholder = "Select near by area"
            self.areaTextfield.textColor = UIColor.lightGray
            self.place = 1

            self.titleLabel.text = "GROCCY MEANS QUALITY"

            
            let screenSize = UIScreen.main.bounds
            if screenSize.height == 812 {
                self.backgroundImage.image = #imageLiteral(resourceName: "login_bg_1x")
            } else {
                self.backgroundImage.image = #imageLiteral(resourceName: "login_bg_1")
            }
        }, completion: nil)
    }
    
    //View1
    @IBAction func tapped_selectPlace(_ sender: Any){
        if (placeTextfield.text == ""){
            AFWrapperClass.alert(Constants.ApplicationName, message: "Please select your city", view: self)
        }
        else{
            UIView.animate(withDuration: 0.5, animations:{
                self.selectAreaView.alpha = 1.0
                self.selectPlaceView.alpha = 0.0
                self.phoneRegisterView.alpha = 0.0
               // self.signInView.alpha = 0.0
                self.backButton.alpha = 1.0
                self.phoneTextfield.text = ""
                self.place = 2
                self.apiGetAllArea()
            }, completion: nil)
        }}
    
    
    
    @IBAction func tapped_selectArea(_ sender: Any) {
        if (areaTextfield.text == ""){
            AFWrapperClass.alert(Constants.ApplicationName, message: "Please select your or near by area", view: self)
        }
        else{
            UIView.animate(withDuration: 0.5, animations:{
                self.selectAreaView.alpha = 0.0
                self.selectPlaceView.alpha = 0.0
                self.phoneRegisterView.alpha = 0.0
                self.selectedAreaView.alpha = 1.0
                self.backButton.alpha = 1.0
                self.phoneTextfield.text = ""
                self.titleLabel.text = "YAY, WE DELIVER AT YOUR DOOR STEP"
                
                
                var area = self.areaTextfield.text
                var place = self.placeTextfield.text

                let str = NSString(format:"%@ , %@", area!, place!)
                
                print(str)
                
                self.selectedPlaceLabel.text = str as String

                let screenSize = UIScreen.main.bounds
                if screenSize.height == 812 {
                    self.backgroundImage.image = #imageLiteral(resourceName: "login_bg_2x")
                } else {
                    self.backgroundImage.image = #imageLiteral(resourceName: "login_bg_2")
                }
            }, completion: nil)
        }}
    
    @IBAction func tappedAction_windowShopping(_ sender: Any) {
        UserDefaults.standard.set(true, forKey: "windowShop")
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "homeController") as! homeController
        self.navigationController?.pushViewController(vc, animated: true)
    }

    @IBAction func tappedAction_loginButton(_ sender: Any) {
            UIView.animate(withDuration: 0.5, animations:{
                self.selectAreaView.alpha = 0.0
                self.selectPlaceView.alpha = 0.0
                self.phoneRegisterView.alpha = 1.0
                self.selectedAreaView.alpha = 0.0

                self.backButton.alpha = 1.0
                self.phoneTextfield.text = ""
                self.titleLabel.text = "YAY, WE DELIVER AT YOUR DOOR STEP"
                
                let screenSize = UIScreen.main.bounds
                if screenSize.height == 812 {
                    self.backgroundImage.image = #imageLiteral(resourceName: "login_bg_2x")
                } else {
                    self.backgroundImage.image = #imageLiteral(resourceName: "login_bg_2")
                }
                
                
            }, completion: nil)
    }
    
    
    
    @IBAction func tapped_signIn(_ sender: Any){
        self.apiOTPLogin()
    }
    
    @IBAction func tapped_conitnueSignIn(_ sender: Any){
             self.apiVeryifyOTPLogin()
    }

    @IBAction func tappedAction_notListedTellUs(_ sender: Any) {
        if !MFMailComposeViewController.canSendMail() {
            print("Mail services are not available")
            return
        }
        sendEmail()
    }
    
    func sendEmail() {
        let composeVC = MFMailComposeViewController()
        composeVC.mailComposeDelegate = self
        // Configure the fields of the interface.
        composeVC.setToRecipients(["support@groccy.com"])
        composeVC.setSubject("I want Groccy too")
        composeVC.setMessageBody("Tell us your name, city, area and phone number. We will be there for you! \n\n Love, Team Groccy.", isHTML: false)
        // Present the view controller modally.
        self.present(composeVC, animated: true, completion: nil)
    }
    

    func mailComposeController(_ controller: MFMailComposeViewController,
                               didFinishWith result: MFMailComposeResult, error: Error?) {
        // Check the result or perform other tasks.
        
        // Dismiss the mail compose view controller.
        controller.dismiss(animated: true, completion: nil)
    }

    
    @IBAction func tappedAction_termscondition(_ sender: Any) {
        if let url = NSURL(string: "https://www.groccy.com/terms-condition.php"){
            UIApplication.shared.openURL(url as URL)
        }
    }
    

    // MARK:  Api Calls
    func apiOTPLogin() {
        guard let phone = phoneTextfield.text, phone != "" else {
            AFWrapperClass.alert(Constants.ApplicationName, message: "Please enter 10 Digit number", view: self)
            return
        }

        let params = [
            
            "phone" : "+91" + phone,
            "area": selectedAreaId ,
            "city": selectedCityId
            
            ] as [String : Any]
        NetworkHelper.sharedInstance.sendAyncronousRequest(method: "auth/otp/request", requestType: "POST", params: params as NSDictionary, showHud: false, completion: { (result) in
            if let dataDict = result as? NSDictionary, let status = dataDict["status"] as? Int, status == 1{
                //let token = dataDict.value(forKey: "token") as? String
                UserDefaults.standard.set(true, forKey: "LoggedIn")
                //UserDefaults.standard.set(token, forKey: "accessToken")
                UserDefaults.standard.set(self.areaTextfield.text, forKey: "selectedArea")
                UIView.animate(withDuration: 0.5, animations:{
                    
                    self.otpLabel.text = "We have sent an OTP to +91 " + phone
                    self.selectPlaceView.alpha = 0.0
                    self.phoneRegisterView.alpha = 0.0
                    self.selectedAreaView.alpha = 0.0
                    self.selectAreaView.alpha = 0.0
                    self.otpView.alpha = 1.0
                    self.backButton.alpha = 1.0
                    DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(3)) {
                        self.pinCodeTextField.becomeFirstResponder()
                    }
                    self.pinCodeTextField.delegate = self
                    self.pinCodeTextField.keyboardType = UIKeyboardType.numberPad
                }, completion: nil)
            }
            else{
                let dataDict = result as? NSDictionary
                let message = dataDict?.value(forKey: "message") as? String
                AFWrapperClass.alert(Constants.ApplicationName, message:message!, view: self)
            }
        }) { (error) in
            AFWrapperClass.alert(Constants.ApplicationName, message: error.localizedDescription, view: self)
        }
    }
    
    func apiVeryifyOTPLogin() {
        guard let phone = phoneTextfield.text, phone != "" else {
            AFWrapperClass.alert(Constants.ApplicationName, message: "Please enter 10 Digit number", view: self)
            return
        }
        guard let pincode = pinCodeTextField.text, phone != "" else {
            AFWrapperClass.alert(Constants.ApplicationName, message: "Please enter 5 Digit OTP", view: self)
            return
        }

        if(UserDefaults.standard.value(forKey:"deviceToken") == nil)
        {
            print("NULL")
            deviceToken = ""
        }else{
            deviceToken  = UserDefaults.standard.value(forKey: "deviceToken") as! String
        }
        
        
        let params = [
            "phone" : "+91" + phone,
            "otp" : pincode,
            "area": selectedAreaId ,
            "city": selectedCityId,
            "deviceType" : "ios",
            "deviceToken": deviceToken
            ] as [String : Any]
        
        NetworkHelper.sharedInstance.sendAyncronousRequest(method: "auth/otp/login", requestType: "POST", params: params as NSDictionary, showHud: false, completion: { (result) in
            if let dataDict = result as? NSDictionary, let status = dataDict["status"] as? Int, status == 1{
                let token = dataDict.value(forKey: "token") as? String
                UserDefaults.standard.set(token, forKey: "accessToken")
                UserDefaults.standard.set(false, forKey: "windowShop")

               if let userEmail = dataDict.value(forKey: "email") as? String{
                    UserDefaults.standard.set(true, forKey: "NoEmail")
                    UserDefaults.standard.set(self.areaTextfield.text, forKey: "selectedArea")
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "homeController") as! homeController
                    self.navigationController?.pushViewController(vc, animated: true)
                }else{
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "myProfile") as! myProfile
                    self.navigationController?.pushViewController(vc, animated: true)
                }
                UserDefaults.standard.set(true, forKey: "LoggedIn")
             }else{
                let dataDict = result as? NSDictionary
                let message = dataDict?.value(forKey: "error") as? String
                AFWrapperClass.alert(Constants.ApplicationName, message:message!, view: self)
            }
        }) { (error) in
            AFWrapperClass.alert(Constants.ApplicationName, message: error.localizedDescription, view: self)
        }
    }
    
    
    func apiGetAllCities() {
        NetworkHelper.sharedInstance.sendAyncronousRequest(method: "city", requestType: "GET", params: nil, showHud: false, completion: { (result) in
            print(result)
            if let dataDict = result as? NSDictionary, let status = dataDict["status"] as? Int, status == 1, let cityArray = dataDict.value(forKey: "cities") as? NSArray{
                print(cityArray)
                for data in cityArray {
                    if let dic  = data as? NSDictionary {
                        var _id = ""
                        if let areaId = dic["_id"] as? String {
                            _id = areaId
                        }
                        var name = ""
                        if let areaName = dic["name"] as? String {
                            name = areaName
                        }
                        self.pickOption.append(AreaModel.init(_id: _id, name: name))
                    }
                }
                let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: self.pickOption)
                UserDefaults.standard.set(encodedData, forKey: "cities")
                UserDefaults.standard.synchronize()
            }
            else{
            }
        }) { (error) in
            AFWrapperClass.alert(Constants.ApplicationName, message: error.localizedDescription, view: self)
        }
    }
    
    
    func apiGetAllArea() {
        let idString = "city/" + selectedCityId + "/areas"
        NetworkHelper.sharedInstance.sendAyncronousRequest(method: idString , requestType: "GET", params: nil, showHud: false, completion: { (result) in
            print(result)
            if let dataDict = result as? NSDictionary, let status = dataDict["status"] as? Int, status == 1, let cityArray = dataDict.value(forKey: "areas") as? NSArray{
                self.arrayOfSectors = SectorModel().getSectorModel(arr: cityArray)
            }
            else{
            }
        }) { (error) in
            AFWrapperClass.alert(Constants.ApplicationName, message: error.localizedDescription, view: self)
        }
    }
}


extension landingController: PinCodeTextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: PinCodeTextField) -> Bool {
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: PinCodeTextField) {
        
    }
    
    func textFieldValueChanged(_ textField: PinCodeTextField) {
        let value = textField.text ?? ""
        print("value changed: \(value)")
    }
    
    func textFieldShouldEndEditing(_ textField: PinCodeTextField) -> Bool {
        return true
    }
    
    func textFieldShouldReturn(_ textField: PinCodeTextField) -> Bool {
        return true
    }
}

