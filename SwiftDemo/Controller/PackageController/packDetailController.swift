//
//  packDetailController.swift
//  SwiftDemo
//
//  Created by Rakesh Kumar on 29/01/18.
//  Copyright © 2018 Rakesh Kumar. All rights reserved.
//

import UIKit
class packDetailController: UIViewController
{
    var packageModel = PackageModel()
 
    @IBOutlet weak var packageNameLabel: UILabel!
    @IBOutlet weak var packageBannerImage: UIImageView!
    @IBOutlet weak var packageShortDescription: UILabel!
    @IBOutlet weak var packageHeading: UILabel!
    @IBOutlet weak var packageDescription: UITextView!
    
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    // MARK:  View Setup
    
    override func viewDidLoad(){
        super.viewDidLoad()
        packageNameLabel.text = packageModel.PackButtonTitle
        packageShortDescription.text = packageModel.PackShortDes
        packageHeading.text = packageModel.PackHeading
        packageDescription.text = packageModel.PackDescription
        self.navigationController?.isNavigationBarHidden = true
        
        if packageNameLabel.text == "GROCCY GOLD PACKAGE" {
            let imageBanner = UIImage(named:"package_gold")
            packageBannerImage.image = imageBanner
        }else if packageNameLabel.text == "GROCCY SILVER PACKAGE"{
            let imageBanner = UIImage(named:"package_silver")
            packageBannerImage.image = imageBanner
        }else{
            let imageBanner = UIImage(named:"package_bronse")
            packageBannerImage.image = imageBanner
        }
    }
   
    // MARK:  Tap Actions Button

    @IBAction func tappedAction_backbutton(_ sender: Any) {
        if let navController = self.navigationController {
            navController.popViewController(animated: true)
        }}
    
    @IBAction func tappedAction_AddToCart(_ sender: Any) {
        self.apiAddPackageCart()
    }

    // MARK:  API Methods
    func apiAddPackageCart() {
        let cartArr = NSMutableArray()
        let params =     ["type" : "package",
                          "package" : packageModel.PackId,
                          "quantity" : "1"]
        cartArr.add(params)

        NetworkHelper.sharedInstance.sendAyncronousRequest(method: "user/cart", requestType: "POST", params: cartArr as Any, showHud: false, completion: { (result) in
            if let dataDict = result as? NSDictionary, let status = dataDict["status"] as? Int, status == 1{
                AFWrapperClass.alert(Constants.ApplicationName, message: "Package added to the cart.", view: self)
            }
            else{
            }
        }) { (error) in
            AFWrapperClass.alert(Constants.ApplicationName, message: error.localizedDescription, view: self)
        }
    }
}
