//
//  communicationController.swift
//  SwiftDemo
//
//  Created by Rakesh Kumar on 31/01/18.
//  Copyright © 2018 Rakesh Kumar. All rights reserved.
//

import UIKit
import Foundation
import MessageUI

class communicationController: UIViewController,MFMailComposeViewControllerDelegate{
    @IBOutlet weak var feedBackTextField: UITextField!

  
    
    override func viewDidLoad(){
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
    }
    
    
    

    @IBAction func tappedAction_emailComposer(_ sender: Any) {
        if !MFMailComposeViewController.canSendMail() {
            print("Mail services are not available")
            return
        }
        sendEmail()
    }

    @IBAction func tappedAction_callExpert(_ sender: Any) {
        let url: NSURL = URL(string: "TEL://1234567890")! as NSURL
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
        } else {
        }
    }
    
    
    @IBAction func tappedAction_feedback(_ sender: Any){
        
    }
    
    @IBAction func tappedAction_menubutton(_ sender: Any) {
        sideMenuVC.toggleMenu()
    }
    
    
    
    func sendEmail() {
        let composeVC = MFMailComposeViewController()
        composeVC.mailComposeDelegate = self
        // Configure the fields of the interface.
        composeVC.setToRecipients(["support@groccy.com"])
        composeVC.setSubject("I want Groccy too")
        composeVC.setMessageBody("Email Customer Support \n\n Love, Team Groccy.", isHTML: false)
        // Present the view controller modally.
        self.present(composeVC, animated: true, completion: nil)
    }
    
    
    func mailComposeController(_ controller: MFMailComposeViewController,
                               didFinishWith result: MFMailComposeResult, error: Error?) {
        // Check the result or perform other tasks.
        
        // Dismiss the mail compose view controller.
        controller.dismiss(animated: true, completion: nil)
    }

}

