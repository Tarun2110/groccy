 //
//  ViewController.h
//  PaymentGateway
//
//  Created by Suraj on 22/07/15.
//  Copyright (c) 2015 Suraj. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PaymentPageViewController : UIViewController<UIWebViewDelegate>

@property (nonatomic,strong)NSString* amount;
@property(nonatomic, strong)NSString * name;
@property (nonatomic,strong)NSString* email;
@property(nonatomic, strong)NSString * phone;
@property(nonatomic, strong)NSString * rechargeId;
@property(nonatomic, strong)NSString * orderId;
@property(nonatomic, strong)NSString * txnId;
@property(nonatomic, assign)BOOL recharge;
@property(nonatomic ,strong)NSString *productInfo;
- (IBAction)backAction:(id)sender;



@end

