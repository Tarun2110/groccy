//
//  itemDetailController.swift
//  SwiftDemo
//
//  Created by Rakesh Kumar on 20/02/18.
//  Copyright © 2018 Rakesh Kumar. All rights reserved.
//
import UIKit

class itemDetailController: UIViewController,UIPickerViewDelegate,UIPickerViewDataSource,UITextFieldDelegate{
    var product = ProductModel()
    var selectedVariant = ""
    
    @IBOutlet weak var productNameLabel: UILabel!
    @IBOutlet weak var productDescription: UILabel!
    @IBOutlet weak var productTotalPriceLabel: UILabel!
    @IBOutlet weak var productPriceLabel: UILabel!
    @IBOutlet weak var productVarientTextField: UITextField!
    @IBOutlet weak var productCountLabel: UILabel!
    @IBOutlet weak var productImage: UIImageView!
    
    var varientPicker = UIPickerView()
    var indexa : Double = 0
    var SellingPrice : Double = 0
    var calculatedPrice : Double = 0

    override func viewDidLoad(){
        super.viewDidLoad()
        indexa += 1

        varientPicker.delegate = self
        varientPicker.dataSource = self
        productVarientTextField.delegate = self
        productVarientTextField.inputView = varientPicker
        
        selectedVariant = product.variants[0]._id
        
        
        productNameLabel.text = product.name
        productPriceLabel.text =  "Rs. " + product.selectedVarient.selling_price
        productTotalPriceLabel.text =  "Total Rs. " + product.selectedVarient.selling_price
        SellingPrice = Double(product.selectedVarient.selling_price)!
        productDescription.text = product.description
        productCountLabel.text = String(format:"%g", indexa)
        
        //Image Banner
        let imageProduct = (product.product_images.count > 0) ? product.product_images[0].image : ""
        productImage.sd_setImage(with: URL(string: imageProduct), placeholderImage: UIImage(named: "placeholder.png"))

        // Textfield Padding and Dropdown setup
        
        let paddingView : UIView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 30))
        let paddingViewR : UIView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 30))
        
        let imageView = UIImageView(frame: CGRect(x: -10, y: 12.5, width: 10, height: 5))
        let image = UIImage(named: "arrow_down")
        imageView.image = image
        imageView.contentMode = .center
        paddingViewR.addSubview(imageView)
        
        productVarientTextField.rightView = paddingViewR
        productVarientTextField.leftView = paddingView
        productVarientTextField.leftViewMode = .always
        productVarientTextField.rightViewMode = .always
        productVarientTextField.text = product.selectedVarient.quantity

        self.navigationController?.isNavigationBarHidden = true
    }
    
    @IBAction func tappedAction_menubutton(_ sender: Any) {
        let _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func tappedAction_minusButton(_ sender: Any){
        if indexa > 1 {
            indexa -= 1
            productCountLabel.text = String(format:"%g", indexa)
            calculatedPrice = SellingPrice * indexa
            productTotalPriceLabel.text = "Total Rs. " + String(format:"%@", calculatedPrice.description)
        }}
    
    @IBAction func tappedAction_addButton(_ sender: Any){
         if indexa < 5 {
            indexa += 1
            productCountLabel.text = String(format:"%g", indexa)
            calculatedPrice = SellingPrice * indexa
            productTotalPriceLabel.text = "Total Rs. " + String(format:"%@", calculatedPrice.description)
        }}
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        variants = product.variants
        varientPicker.reloadAllComponents()
        return true
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return product.variants.count
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return product.variants[row].quantity
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        productVarientTextField.text =   product.variants[row].quantity
        productPriceLabel.text = "Rs. " + product.variants[row].selling_price
        productTotalPriceLabel.text = product.variants[row].selling_price
        productTotalPriceLabel.text =  "Total Rs. " + product.variants[row].selling_price
        if product.variants[row].selling_price != "" , let value = Double(product.variants[row].selling_price) {
        SellingPrice = value
        selectedVariant  = product.variants[row]._id
        }
    }
    
    @IBAction func tappedAction_addToCart(_ sender: Any) {
            let cartArr = NSMutableArray()
            let params = ["type" : "product",
                          "product" : product.productID,
                          "variant" : selectedVariant,
                          "quantity" : String(format:"%g", indexa)]
            cartArr.add(params)
            NetworkHelper.sharedInstance.sendAyncronousRequest(method: "user/cart", requestType: "POST", params: cartArr as Any, showHud: false, completion: { (result) in
                if let dataDict = result as? NSDictionary, let status = dataDict["status"] as? Int, status == 1{
                    if let cartArray = dataDict.value(forKey: "cart") as? NSArray {
                        let cartCount = cartArray.count
                        UserDefaults.standard.set(cartCount.description, forKey: "cartCount")
                    }
                    for item in 0..<products.count{
                        products[item].count = 0
                    }
                    AFWrapperClass.alert(Constants.ApplicationName, message: "Item added to the cart", view: self)
                }
                else{
                }
            }) { (error) in
                AFWrapperClass.alert(Constants.ApplicationName, message: error.localizedDescription, view: self)
            }
    }
}

