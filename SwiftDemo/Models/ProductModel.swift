//
//  ProductModel.swift
//  SwiftDemo
//
//  Created by Rakesh Kumar on 23/03/18.
//  Copyright © 2018 Rakesh Kumar. All rights reserved.
//

import Foundation

struct ProductModel {
    var productID = ""
    var name = ""
    var hindi_name = ""
    var description = ""
    var keyword = ""
    var type = ""
    var hsn_code = ""
    var category = CategoryModel()
    var sub_category = CategoryModel()
    var brand = BrandModel()
    var variants = [VariantsModel]()
    var tax = TaxModel()
    var product_images = [Product_imagesModel]()
    var __v = 0
    var count = 0
    var selectedVarient = VariantsModel()
    
    
  

    var quantity = 0
    var packName = ""
    var packheading = ""
    var icon_image = ""
    var short_description = ""
    var packId = ""

    
    func getPackageModel(arr: NSArray) -> [ProductModel] {
        var products = [ProductModel]()
        for data in arr {
            if let dict = data as? NSDictionary {
                products.append(ProductModel().getSingleProduct(dict: dict))
            }}
        return products
    }
    
    func getSingleProduct(dict: NSDictionary) -> ProductModel {
        var product = ProductModel()
        if let BId = dict["_id"] as? String {
            product.productID = BId
        }
        if let BId = dict["id"] as? String {
            product.productID = BId
        }
        
        if let name = dict["name"] as? String {
            product.name = name
        }
        if let hindi_name = dict["hindi_name"] as? String {
            product.hindi_name = hindi_name
        }
        if let keyword = dict["keyword"] as? String {
            product.keyword = keyword
        }
        if let type = dict["type"] as? String {
            product.type = type
        }
        if let hsn_code = dict["hsn_code"] as? String {
            product.hsn_code = hsn_code
        }
        if let __v = dict["__v"] as? Int {
            product.__v = __v
        }
        if let category = dict["category"] as? NSDictionary {
            product.category = CategoryModel().getCategoryModel(dict: category)
        }
        if let sub_category = dict["sub_category"] as? NSDictionary {
            product.sub_category = CategoryModel().getCategoryModel(dict: sub_category)
        }
        
        if let sub_category = dict["product"] as? NSDictionary {
            product.sub_category = CategoryModel().getCategoryModel(dict: sub_category)
        }
        
        if let brand = dict["brand"] as? NSDictionary {
            product.brand = BrandModel().getBrandModel(dict: brand)
        }
        if let variants = dict["variants"] as? NSArray {
            product.variants = VariantsModel().getVarientsModels(arr: variants)
            product.selectedVarient = product.variants[0]
        }
        if let tax = dict["tax"] as? NSDictionary {
            product.tax = TaxModel().getTaxModel(dict: tax)
        }
        if let product_images = dict["product_images"] as? NSArray {
            product.product_images = Product_imagesModel().getImageModel(arr: product_images)
        }
        return product
    }
    
    func getSinglePackage(dict: NSDictionary) -> ProductModel {
        var product = ProductModel()
        if let quantity = dict["price"] as? Int {
            product.quantity = quantity
        }
        
        if let name = dict["name"] as? String {
            product.packName = name
        }
        
        if let heading = dict["heading"] as? String {
            product.packheading = heading
        }
        
        if let iconimage = dict["icon_image"] as? String {
            product.icon_image = iconimage
        }
      
        if let shortdescription = dict["short_description"] as? String {
            product.short_description = shortdescription
        }
        
        if let id = dict["id"] as? String {
            product.packId = id
        }
        return product
        
    }
}

struct CategoryModel {
    var parent = ""
    var icon  = ""
    var _id = ""
    var name = ""
    var quantity = 0
     var price = 0
    var __v = 0
    
    func getCategoryModel(dict: NSDictionary) -> CategoryModel {
        var category = CategoryModel()
        if let parent = dict["parent"] as? String {
            category.parent = parent
        }
        if let icon = dict["icon"] as? String {
            category.icon = icon
        }
        if let _id = dict["_id"] as? String {
            category._id = _id
        }
        if let name = dict["name"] as? String {
            category.name = name
        }
      
        if let quantity = dict["quantity"] as? Int {
            category.quantity = quantity
        }
        
        if let price = dict["price"] as? Int {
            category.price = price
        }
        
       
        
        if let __v = dict["__v"] as? Int {
            category.__v = __v
        }
        return category
    }
}

struct TaxModel {
    var sgst = 0
    var cgst = 0
    var _id = ""
    var __v = 0
    
    func getTaxModel(dict: NSDictionary) -> TaxModel {
        var Taxcategory = TaxModel()
        if let sgst = dict["sgst"] as? Int {
            Taxcategory.sgst = sgst
        }
        if let cgst = dict["cgst"] as? Int {
            Taxcategory.cgst = cgst
        }
        if let _id = dict["_id"] as? String {
            Taxcategory._id = _id
        }
        if let __v = dict["__v"] as? Int {
            Taxcategory.__v = __v
        }
        return Taxcategory
    }
}

struct BrandModel {
    var _id = ""
    var name = ""
    var __v = 0
    
    func getBrandModel(dict: NSDictionary) -> BrandModel {
        var BrandCategory = BrandModel()
        if let _id = dict["_id"] as? String {
            BrandCategory._id = _id
        }
        if let name = dict["name"] as? String {
            BrandCategory.name = name
        }
        if let __v = dict["__v"] as? Int {
            BrandCategory.__v = __v
        }
        
        return BrandCategory
    }
    
}

struct VariantsModel {
    var _id = ""
    var quantity = ""
    var mrp = ""
    var discount = ""
    var selling_price = ""
    var product = ""
    var __v = 0
    
    func getVarientsModels(arr: NSArray) -> [VariantsModel] {
        var varients = [VariantsModel]()
        for data in arr {
            if let dict = data as? NSDictionary {
                varients.append(VariantsModel().getVariantModel(dict: dict))
            }
        }
        return varients
    }
    
    func getVariantModel(dict: NSDictionary) -> VariantsModel {
        var VariantCategory = VariantsModel()
       
        if let _id = dict["_id"] as? String {
            VariantCategory._id = _id
        }
        if let quantity = dict["quantity"] as? String {
            VariantCategory.quantity = quantity
        }
        if let mrp = dict["mrp"] as? String {
            VariantCategory.mrp = mrp
        }
        
        if let discount = dict["discount"] as? String {
            VariantCategory.discount = discount
        }
        if let selling_price = dict["selling_price"] as? String {
            VariantCategory.selling_price = selling_price
        }
        if let product = dict["product"] as? String {
            VariantCategory.product = product
        }
        if let __v = dict["__v"] as? Int {
            VariantCategory.__v = __v
        }
        return VariantCategory
    }
    
}
struct Product_imagesModel {
  var image = ""
    
    func getImageModel(arr: NSArray) -> [Product_imagesModel] {
        var ImageBannerCategory = [Product_imagesModel]()
        for data in arr {
            if let img = data as? String {
                var im = Product_imagesModel()
                im.image = img
                ImageBannerCategory.append(im)
            }
        }
        return ImageBannerCategory
    }
    
}
