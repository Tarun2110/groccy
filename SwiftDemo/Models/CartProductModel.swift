//
//  ProductModel.swift
//  SwiftDemo
//
//  Created by Rakesh Kumar on 23/03/18.
//  Copyright © 2018 Rakesh Kumar. All rights reserved.
//

import Foundation

struct CartProductModel {
    var productID = ""
    var quantity = 0
    var selectedVarient = VariantsModel()
    var user = ""
    var type = ""
    var __v = 0
    var totalAmount = 0

    
    var product = ProductModel()
    var packages = ProductModel()
    
    func getPackageModel(arr: NSArray) -> [CartProductModel] {
        var products = [CartProductModel]()
        for data in arr {
            if let dict = data as? NSDictionary {
                var productObj = CartProductModel()
                if let BId = dict["_id"] as? String {
                    productObj.productID = BId
                }
                if let quantity = dict["quantity"] as? Int {
                    productObj.quantity = quantity
                }
                
                if let amount = dict["amount"] as? Int {
                    productObj.totalAmount = amount
                }
                
                if let type = dict["type"] as? String {
                    productObj.type = type
                }
                if let variant = dict["variant"] as? NSDictionary {
                    productObj.selectedVarient = VariantsModel().getVariantModel(dict: variant)
                }
                if let __v = dict["__v"] as? Int {
                    productObj.__v = __v
                }
                
                if let product = dict["product"] as? NSDictionary {
                    productObj.product = ProductModel().getSingleProduct(dict: product)
                }
                
                if let product = dict["package"] as? NSDictionary {
                    productObj.packages = ProductModel().getSinglePackage(dict: product)
                }
                products.append(productObj)
            }}
        return products
    }
}

